var fs = require('fs')
if (!fs.existsSync('./hivs')) fs.mkdirSync('./hivs');

require('http').createServer((req,res)=>{
  req.url=req.url.replace(/\/+/g,'/').replace(/\/+$/,'')
  if (req.url == '/hivs' && req.method == 'POST'){
    var fileName = `${Date.now()}-${~~(Math.random()*10000000)}.hiv`
    req.pipe(fs.createWriteStream('./hivs/'+fileName));
    req.on('end',()=>res.end(JSON.stringify({fileName:fileName})));
  } else if (req.url == '/hivs' && req.method == 'GET')
    fs.readdir('./hivs',(err,r)=>res.end(JSON.stringify({files:r.sort((a,b)=>a>b?-1:(a<b?1:0)).slice(0,5)})));
  else if (req.url.indexOf('/hivs/')==0 && req.method == 'GET')
    fs.readdir('./hivs',(err,r)=>{
      var file = req.url.replace(/^\/hivs\/+/,'');
      if (r.indexOf(file)!=-1){
        var size = fs.stat('./hivs/'+file,(err,r)=>{
          res.writeHead(200, {'content-length':r.size, 'content-type':'application/octet-stream'});
          var f = fs.createReadStream('./hivs/'+file).pipe(res);
          f.on('end',()=>res.end());
        })
      } else
        res.writeHead(404),res.end();
    });
  else 
    res.writeHead(400),res.end(),console.log('invalid url',req.url);
}).listen(8180);
