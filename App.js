/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

/*

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to React Native!</Text>
        <Text style={styles.instructions}>To get started, edit App.js</Text>
        <Text style={styles.instructions}>{instructions}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

*/


/*
import React, { Component } from 'react';
import {
AppRegistry,
StyleSheet,
Text,
View,
Linking,
Vibration,
Dimensions
} from 'react-native';

import Camera from 'react-native-camera';

export default class ReactBarcodeScannerProject extends Component {

_handleBarCodeRead(e) {
    Vibration.vibrate();
    this.setState({scanning: false});    
    Linking.openURL(e.data).catch(err => console.error('An error occured', err));
    return;
}  
getInitialState() {
        return {
            scanning: true,
            cameraType: Camera.constants.Type.back
        }
}   
render() {
    if(this.state.scanning) {
    return (
    <View style={styles.container}>
        <Text style={styles.welcome}>
        Barcode Scanner
        </Text>
        <View style={styles.rectangleContainer}>
        <Camera style={styles.camera} type={this.state.cameraType} onBarCodeRead={this._handleBarCodeRead.bind(this)}>
            <View style={styles.rectangleContainer}>
            <View style={styles.rectangle}/>
            </View>            
        </Camera>
        </View>
        <Text style={styles.instructions}>
        Double tap R on your keyboard to reload,{'\n'}
        </Text>
    </View>
    );
    }
    else{
    return (<View  style={styles.container}>
        <Text style={styles.welcome}>
        Barcode Scanner
        </Text>      
        <Text style={styles.instructions}>
        Double tap R on your keyboard to reload,{'\n'}
        </Text>     
    </View>);
    }
}
}

const styles = StyleSheet.create({
container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
},
camera: {
    flex: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    height: Dimensions.get('window').width,
    width: Dimensions.get('window').width,
},  
welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
},
instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
},
rectangleContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
},

rectangle: {
    height: 250,
    width: 250,
    borderWidth: 2,
    borderColor: '#00FF00',
    backgroundColor: 'transparent',
},  
});

AppRegistry.registerComponent('ReactBarcodeScannerProject', () => ReactBarcodeScannerProject);   
*/

/*

'use strict';

import React, { Component } from 'react';

import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  Linking,
} from 'react-native';

import QRCodeScanner from 'react-native-qrcode-scanner';

class ScanScreen extends Component {
  onSuccess = (e) => {
    Linking
      .openURL(e.data)
      .catch(err => console.error('An error occured', err));
  }

  render() {
    return (
      <QRCodeScanner
        onRead={this.onSuccess}
        topContent={
          <Text style={styles.centerText}>
            Go to <Text style={styles.textBold}>wikipedia.org/wiki/QR_code</Text> on your computer and scan the QR code.
          </Text>
        }
        bottomContent={
          <TouchableOpacity style={styles.buttonTouchable}>
            <Text style={styles.buttonText}>OK. Got it!</Text>
          </TouchableOpacity>
        }
      />
    );
  }
}

const styles = StyleSheet.create({
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777',
  },
  textBold: {
    fontWeight: '500',
    color: '#000',
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)',
  },
  buttonTouchable: {
    padding: 16,
  },
});

AppRegistry.registerComponent('default', () => ScanScreen);*/


//This is an example code to Scan QR code//
import React, { Component } from 'react';
//import react in our code.
import { BackHandler, Alert, Text, View, Linking, TouchableHighlight, PermissionsAndroid, Platform, StyleSheet} from 'react-native';
// import all basic components
import { Button, Input } from 'react-native-elements';
import { Scanner } from './screens/Scanner';
import { Browser } from './screens/Browser';
import { Passphrase } from './screens/Passphrase';
import { RealmSwitcher } from './screens/RealmSwitcher';
import { DataMgr } from './screens/DataMgr'
import { Misc } from './screens/Misc'
import { RealmMenu } from './screens/RealmMenu'
import { ExportManager } from './screens/ExportManager'
import { ImportManager } from './screens/ImportManager'
var RNFS = require('react-native-fs');



export default class HiveIPAM extends Component {
  constructor() {
    super();
    this.state = {
      realm: -1,
      cview:'main',
      inventory: null,
      realms: null,
      settings: {},
      passphrase: null,
      itemCode: null
    };
  }
  
  loadData(realm, cb) {
    if (realm == -1) return;

    const key = `${realm}:inventory`;
    const msgLine1 = "No inventory found";
    const msgLine2 = `for realm ${realm}. Create new one?`;
    const def = {info:{},hwList:[]};

    DataMgr.loadAsyncStorage(this.state.passphrase,key,msgLine1, msgLine2, def, (data)=>{
      if (data === null)
        this.setState({realm: -1}, ()=>{cb(false)});
      else
        this.setState({inventory: data}, ()=>{cb(true)});
    });
  }
  storeData(realm, data, cb) {
    if (realm == -1) return;

    const key = `${realm}:inventory`;
    DataMgr.storeAsyncStorage(this.state.passphrase,key,data,()=>{
      Alert.alert("Success",`Inventory for realm ${realm} saved`);
      cb(true);
    });
  }
  removeData(realm, cb){
    if (realm == -1) return;

    const key = `${realm}:inventory`;
    DataMgr.removeAsyncStorage(key,()=>{
      Alert.alert("Success",`Inventory for realm ${realm} removed`);
      cb(true);
    });
  }
  loadRealms(cb) {
    const key = "userdata";
    const msgLine1 = "No user data found";
    const msgLine2 = "Init new application data?";
    const def = {settings:{},realms:[]};

    DataMgr.loadAsyncStorage(this.state.passphrase,key,msgLine1, msgLine2, def, (data)=>{
      if (data === null)
        this.setState({passphrase: null, settings:{}},()=>{cb(false)});
      else
        this.setState({realms: data.realms, settings: data.settings},()=>{cb(true)});
    });
  }
  storeRealms(data,cb) {
    const key = "userdata";
    DataMgr.storeAsyncStorage(this.state.passphrase,key,data,()=>{
      cb(true);
    });
  }
  setPassphrase(pass) {
    this.setState({passphrase:pass},()=>{
      this.loadRealms((ret)=>{
        if (!ret)
          Alert.alert("Error","Invalid passphrase");
      });
    })
  }
  addRealm(newRealm) {
    if (this.state.realms.indexOf(newRealm) == -1){
      this.setState({realms: [...this.state.realms, newRealm]},()=>{
        this.storeRealms({realms:this.state.realms, settings:this.state.settings},()=>{});
      });
    } else
      Alert.alert("Error","Realm with this name already exists");
  }
  delRealm(realm) {
    var that = this;
    var realmName = that.state.realms[realm];
    Alert.alert(
      `Remove realm ${realmName} inventory?`,
      "This operation cannot be undone",
      [
        {text: 'Yeh', onPress: async () => {
          that.setState({realms: that.state.realms.filter((s,i)=>i!=realm)},()=>{
            that.storeRealms({realms:that.state.realms, settings:that.state.settings}, ()=>{
              that.removeData(realmName,()=>{
                Alert.alert("Success",`Realm ${realmName} removed`);
              });
            });
          });
        }},
        {text: 'Nuh-uh'}
      ],
      {cancellable:false}
    )
  }
  selectRealm(realm) {
    const realmName = this.state.realms[realm];
    this.setState({realm: realm}, ()=>{
      this.loadData(realmName,(s)=>{});
    }); 
  }
  editData(data) {
    var realmName = this.state.realms[this.state.realm];
    this.setState({inventory: data},()=>{
      this.storeData(realmName, data, ()=>{});
    });
  }

  componentWillMount() {
    //DataMgr.clear();
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      if (this.state.inventory){
        this.setState({inventory: null, realm: -1});
        return true;
      } else
        return false;
    });
    //console.disableYellowBox = true;
    console.warn('warnings are enabled');

    /*Misc.getPermission([
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE
      ],(res)=>{
        var path = RNFS.ExternalStorageDirectoryPath+'/test.hiv'
        RNFS.writeFile(path, 'Pussy', 'utf8')
          .then((success)=>{

          }).catch((err)=>{
            alert("File write error "+err);
          });
      });*/
  }
  componentWillUnmount() {
    this.backHandler.remove();
  }

  render() {
    let displayModal;
    //If qrvalue is set then return this view
    if (this.state.inventory == null) {
      if (this.state.passphrase == null) {
        return (
          <Passphrase
            onInput={(pass) => this.setPassphrase(pass)}/>
        )
      } else if (this.state.realms == null) {
        return (
          <View style={styles.container}>
            <Text style={styles.heading}>Reading realms...</Text>
          </View>
        )
      } else if (this.state.realm == -1)
        return (
          <RealmSwitcher
            realms={this.state.realms}
            onAddRealm={(newRealm) => this.addRealm(newRealm)}
            onDelRealm={(realm) => this.delRealm(realm)}
            onSelectRealm={(realm) => this.selectRealm(realm)}/>
        )
      else
        return (
          <View style={styles.container}>
            <Text style={styles.heading}>Reading realm {this.state.realms[this.state.realm]}...</Text>
          </View>
        )
    } else if (this.state.cview == 'scanner') {
      return (
        <Scanner 
          onReadCode={(code) => this.setState({cview:code?'browser':'main', itemCode: code})}/>
      );
    } else if (this.state.cview == 'browser') {
      return (
        <Browser
          realmName={this.state.realms[this.state.realm]}
          inventory={this.state.inventory}
          item={this.state.itemCode}
          onEdit={(newInventory) => this.editData(newInventory)}
          onDone={(status) => this.setState({cview:'main', itemCode:null})}/>
      );
    } else if (this.state.cview == 'main') {
      return (
        <RealmMenu
          realmName={this.state.realms[this.state.realm]}
          inventory={this.state.inventory}
          onViewChange={(view)=>view?this.setState({cview:view}):this.setState({inventory: null, realm: -1})}/>
      );
    } else if (this.state.cview == 'export') {
      return (
        <ExportManager
          realmName={this.state.realms[this.state.realm]}
          inventory={this.state.inventory}
          onDone={(status) => this.setState({cview:'main', itemCode:null})}/>
      );
    } else if (this.state.cview == 'import') {
      return (
        <ImportManager
          realmName={this.state.realms[this.state.realm]}
          inventory={this.state.inventory}
          onImport={(newInventory, code)=>this.editData(newInventory)}
          onDone={(code) => this.setState(code?{cview:'browser',itemCode:code}:{cview:'main', itemCode:null})}/>
      );
    } else 
      return (
        <View style={styles.container}>
            <Text style={styles.heading}>:(</Text>
          </View>
      )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'white'
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#2c3539',
    padding: 10,
    width:300,
    marginTop:16
  },
  heading: { 
    color: 'black', 
    fontSize: 24, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 30 
  },
  simpleText: { 
    color: 'black', 
    fontSize: 20, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 16
  }
});