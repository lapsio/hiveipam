import React, { Component } from 'react';
import { FlatList, ScrollView, BackHandler, Alert, Text, View, Linking, TouchableHighlight, PermissionsAndroid, Platform, StyleSheet} from 'react-native';
import { Button, Input, ListItem } from 'react-native-elements';

const portdefaults = {
  medium:'sfp',
  type:'data'
}

export class DevicePortGroup extends Component {
  constructor() {
    super();
    this.state = {
    };
  }

  clashPorts(section){
    var o = {};
    if (section.type=='portgroup'){
      o[section.gid]={};
      for (var i = 0 ; i < section.width*section.height ; ++i)
        o[section.gid][i+section.sid]=section.portdefaults;
    } else { 
      section.content.map(c=>this.clashPorts(c)).forEach(c=>{
        for (var group in c)
          for (var port in c[group])
            (o[group]||(o[group]={}))[port]=c[group][port]; //TODO: IMPLEMENT COMBO
      });
    }
    return o;
  }
  getPortInfo(){
    var o3 = this.props.device.portinfo,
        o2 = this.clashPorts(this.props.device),
        o1 = portdefaults;
        
    return [o1,o2,o3];
  }
  mergePort(group, port, defaults, groupdefaults, overrides){
    var portinfo = {},
        groupdefaults = groupdefaults[group][port],
        overrides = ((overrides[group]||{})[port]||{});

    portinfo._src={};
    portinfo._src.overrides = Object.keys(overrides);
    portinfo._src.groupdefaults = Object.keys(groupdefaults)
      .filter(k=>!portinfo._src.overrides.includes(k));
    portinfo._src.defaults = Object.keys(portdefaults)
      .filter(k=>!portinfo._src.overrides.includes(k) &&
                 !portinfo._src.groupdefaults.includes(k));
    
    portinfo = {...portinfo, ...defaults, ...groupdefaults, ...overrides};

    return portinfo;
  }
  getInfoSrc(mergedPort, prop){
    return (mergedPort._src.defaults.includes(prop)?'defaults':
      mergedPort._src.groupdefaults.includes(prop)?'groupdefaults':
        mergedPort._src.overrides.includes(prop)?'overrides':'unknown');
  }
  showInfo(group, port){
    var portinfo = this.getPortInfo(),
        mergedPort = this.mergePort(group, port, portinfo[0], portinfo[1], portinfo[2]);

    Alert.alert(
      `Port info: ${group}.${port}`,
      Object.keys(mergedPort)
        .filter(k=>k!='_src')
        .map(k=>`[${this.getInfoSrc(mergedPort,k)[0].toUpperCase()}] ${k}: ${mergedPort[k]}`)
      .concat([
        '',
        '[D]efaults, [G]roup defaults, [O]verrides'
      ]).join('\n')
    );
  }
  componentWillMount() {
    /*this.backHandler = BackHandler.addEventListener('hardwareBackPress', ()=>{
      this.props.callback(null); 
      return true;});*/
  }
  componentWillUnmount() {
    /*this.backHandler.remove();*/
  }
  
  render() {
    let displayModal;
    //If qrvalue is set then return this view
    var w = this.props.direction=='row'?this.props.height:this.props.width,
        h = this.props.direction=='row'?this.props.width:this.props.height,
        revsub = this.props.direction=='row'^([this.props.direction=='row'?'horizontally':'vertically','both'].indexOf(this.props.flip)!=-1),
        revports = ([this.props.direction=='row'?'vertically':'horizontally','both'].indexOf(this.props.flip)!=-1),
        ports = [];
    for (var i = 0 ; i < w ; ++i){
      var sub = [];
      for (var j = 0 ; j < h; ++j)
        sub.push([
          this.props.groupId,
          this.props.startId+i*h+
            (!revsub?h-j-1:j)]);
      ports.push(sub);
    }

    if (this.props.direction=='row')
      ports = ports.reverse();
    if (revports)
      ports = ports.reverse();
        
    return (
      <View style={{
        justifyContent:'center',
        alignItems:'center',
        flex:this.props.size,
        borderColor:this.props.device.color,
        borderBottomWidth:1,
        borderRightWidth:1
      }}>
        <View
          style={{
            alignSelf:'center',
            width:this.props.height*25-this.props.level-this.props.height,
            height:this.props.width*25-this.props.level-this.props.height,
            backgroundColor:'#f0f',
            flexDirection:this.props.direction
          }}>
          {
            ports.map((s,i)=>(
              <View
                style={{
                  flex:1,
                  backgroundColor:'#ddd',
                  flexDirection:this.props.direction=='row'?'column':'row'
                }}
                key={i}>
                {
                  s.map((ss,ii)=>(
                    <TouchableHighlight
                      style={{
                        flex:1,
                        borderWidth:1,
                        borderColor:'#000'
                      }}
                      key={i*this.props.height+ii}
                      onPress={()=>this.showInfo(ss[0],ss[1])}>
                      <Text
                        style={{fontSize:10,alignSelf:'center'}}>
                        {ss[1]}
                      </Text>
                    </TouchableHighlight>
                  ))
                }
              </View>
            ))
          }
        </View>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'white'
  },
   inputrow: {
    flexDirection: 'row',
    paddingHorizontal:30,
    paddingBottom: 15
  },
  inputbutton: {
    alignItems: 'center',
    backgroundColor: '#2c3539',
    paddingVertical: 10,
    paddingHorizontal: 30,
    marginTop:16
  },
  inputinput: {
    flex:1,
    marginTop:16,
    marginRight:20
  },
  camButton: {
    position: 'relative',
    alignItems: 'center',
    padding: 10,
    width:200,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#2c3539',
    padding: 10,
    width:300,
    marginTop:16
  },
  heading: { 
    color: 'black', 
    fontSize: 24, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 30 
  },
  simpleText: { 
    color: 'black', 
    fontSize: 20, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 16
  }
});