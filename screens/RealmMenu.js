import React, { Component } from 'react';
import { BackHandler, Alert, Text, View, Linking, TouchableHighlight, PermissionsAndroid, Platform, StyleSheet} from 'react-native';
import { Header, Button } from 'react-native-elements';
import { ButtonMenu } from './ButtonMenu'

const viewsMap=[
  'browser',
  'scanner',
  'export',
  'import'
]
const viewsNames=[
  'Inventory',
  'Scan device',
  'Export device bundle',
  'Import device bundle'
]

export class RealmMenu extends Component {
  constructor() {
    super();
    this.state = {
    };
  }
  render() {
    return (
      <View style={{flex:1}}>
        <Header
          statusBarProps={{backgroundColor:'#089',translucent:true}}
          containerStyle={{backgroundColor:'#3ab'}}
          placement="left"
          leftComponent={{ icon: 'arrow-back', color: '#fff', underlayColor:'transparent' , onPress:()=>this.props.onViewChange(null)}}
          centerComponent={{text:`${this.props.realmName}`, style:{fontSize:20,color:'white'}}}
          />
        <ButtonMenu
            buttons={viewsNames}
            onSelect={(i)=>this.props.onViewChange(viewsMap[i])}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'white'
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#2c3539',
    padding: 10,
    width:300,
    marginTop:16
  },
  heading: { 
    color: 'black', 
    fontSize: 24, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 30 
  },
  simpleText: { 
    color: 'black', 
    fontSize: 20, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 16
  }
});