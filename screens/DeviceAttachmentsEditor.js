import React, { Component } from 'react';
import { Animated, StatusBar, Image, FlatList, ScrollView, BackHandler, Alert, Text, View, Linking, TouchableHighlight, PermissionsAndroid, Platform, StyleSheet} from 'react-native';
import { CheckBox, Icon, Slider, Button, Input, ListItem } from 'react-native-elements';
import { DeviceCredentialsEditor } from './DeviceCredentialsEditor';
import { Collapsible } from './Collapsible';
import ImagePicker from 'react-native-image-picker';
import FilePickerManager from 'react-native-file-picker';
import FlexImage from 'react-native-flex-image';
import { FileMisc } from './Misc';
import Overlay from 'react-native-modal-overlay';
import FileViewer from 'react-native-file-viewer';
import { Misc } from './Misc'


var RNFS = require('react-native-fs');

export class DeviceAttachmentsEditor extends Component {
  constructor() {
    super();
    this.state = {
      copyDisabled:true,
      devPath:null,
      selectedAttachments:null,
      preview:null,
      imagesAsFiles:false,
      filesAsImages:false,
      zoomPreview:1,
      zoomPreviewValue:new Animated.Value(1)
    };
  }
  updateSelf(self, silent) {
    for (var key in self)
      this.props.device[key]=self[key];
    if (!silent)
      this.props.onEdit();
  }
  collapseToggle(key) {
    var newState={};
    newState[key]=!this.state[key];
    this.setState(newState);
  }
  handleFile(response, cb){
    if (response.error) 
      return Alert.alert(
        'Error',
        'Error during file read: '+response.error
      );
    if (response.didCancel || response.didRequestPermission || !response.uri) 
      return;

    var fileMeta = {};
    for (var key in response)
      if (key!='data')
        fileMeta[key]=response[key];

    var localPath = this.state.devPath+Date.now()+'-'+(~~(Math.random()*1000000000));
    fileMeta.localPath = localPath;
    RNFS.copyFile(response.uri,localPath)
      .then((success)=>{
        cb(fileMeta);
      })
      .catch((err)=>{
        cb(null,err);
      })
  }
  pickImage(camera) {
    Misc.getPermission([
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.CAMERA
      ],(res)=>{
        if (res)
          ImagePicker[camera?'launchCamera':'launchImageLibrary']({title: 'Add image'}, (response) => {
            this.handleFile(response, (newFile,err)=>{
              if (newFile){
                this.props.device.images.push(newFile);
                this.props.onEdit();
              } else
                Alert.alert(
                  'Error',
                  'Could not copy file to local storage '+err
                );
            })
          })
        else
          Alert.alert(
            'Notice',
            'Required permissions have not been granted'
          );
      })
  }
  pickFile() {
    Misc.getPermission([
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.CAMERA
      ],(res)=>{
        if (res)
          FilePickerManager.showFilePicker((response)=>{
            this.handleFile(response, (newFile,err)=>{
              if (newFile){
                this.props.device.files.push(newFile);
                this.props.onEdit();
              } else
                Alert.alert(
                  'Error',
                  'Could not copy file to local storage '+err
                );
            });
          })
        else
          Alert.alert(
            'Notice',
            'Required permissions have not been granted'
          );
    })
  }
  cleanupAttachments() {
    RNFS.readdir(this.state.devPath)
      .then((success)=>{
        var fullPaths = success.map(s=>this.state.devPath+s);
          var ia = this.props.device.images.length,
              fa = this.props.device.files.length;
          this.props.device.images = this.props.device.images
            .filter(i=>fullPaths.filter(p=>p==i.localPath).length>0);
          this.props.device.files = this.props.device.files
            .filter(i=>fullPaths.filter(p=>p==i.localPath).length>0);
          var ib = this.props.device.images.length,
              fb = this.props.device.files.length;
          if (ia-ib + fa - fb > 0)
            Alert.alert(
              'Notice',
              `${ia-ib + fa - fb} attachments entries have been removed due to missing files`
            );
          
          fullPaths.filter(p=>(
            this.props.device.images
              .filter(i=>i.localPath==p).length==0 && 
            this.props.device.files
              .filter(f=>f.localPath==p).length==0))
          .forEach(p=>{
            RNFS.unlink(p)
              .then((success)=>{
                console.warn('garbage file removed '+p);
              }).catch((err)=>{
                console.warn('Could not cleanup file '+err);
              })
          })
          this.props.onEdit();
      }).catch((err)=>{
        console.warn(err);
      })
  }
  selectAttachment(s){
    var index = this.state.selectedAttachments.indexOf(s),
        backBind = ()=>{
          if (!this.backHandler && this.state.selectedAttachments.length != 0)
            this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
              this.setState({selectedAttachments:[]});
              this.props.onEdit();
              return true;
            });
          else if (this.backHandler && this.state.selectedAttachments.length == 0){
            this.backHandler.remove();
            this.backHandler=null;
          }
        };
        
    if (index==-1){
      this.setState({selectedAttachments:[...this.state.selectedAttachments,s]},backBind);
    } else {
      this.setState({selectedAttachments:this.state.selectedAttachments.filter(i=>i!=s)},backBind)
    }
  }
  deleteAttachments(){
    var selected = this.state.selectedAttachments;
    this.props.device.images = this.props.device.images.filter(i=>this.state.selectedAttachments.indexOf(i)==-1);
    this.props.device.files = this.props.device.files.filter(i=>this.state.selectedAttachments.indexOf(i)==-1);
    this.setState({selectedAttachments:[]},()=>{
      if (this.backHandler)
        this.backHandler.remove();
      this.props.onEdit();
    });
  }
  openImage(s){
    this.setState({preview:s,showPreview:true});
  }
  openFile(s){
    FileViewer.open(s.localPath)
      .then((success)=>{
        
      }).catch((err)=>{
        Alert.alert(
          'Error',
          'Could not open file locally '+err
        );
      })
  }
  componentWillMount() {
    /*this.backHandler = BackHandler.addEventListener('hardwareBackPress', ()=>{
      this.props.callback(null); 
      return true;});*/
      this.setState({
        selectedAttachments:[],
        devPath:RNFS.DocumentDirectoryPath+'/attachments/'+this.props.realmName+'/'+this.props.device.code+'/'
      },()=>{
        RNFS.mkdir(this.state.devPath)
          .then((success)=>{
            this.setState({copyDisabled:false});
            this.cleanupAttachments();
          }).catch((err)=>{
            Alert.alert(
              'Error',
              'Could not create attachments repository. Attachments operations will be disabled'
            );
          });
      })
  }
  componentWillUnmount() {
    /*this.backHandler.remove();*/
    //this.cleanupImages();
  }
  
  render() {
    let displayModal;
    //If qrvalue is set then return this view
    return (
      <Collapsible
        beginCollapsed={false}
        style={styles.collapsible}
        titleStyle={styles.collapsibleHead}
        buttonStyle={{padding:16}}
        title="Manage attachments >">
        <View style={{flex:1,flexDirection:'row'}}>
          <Icon
            disabled={this.state.copyDisabled}
            color='#3ab'
            reverse
            name='image'
            onPress={()=>this.pickImage(false)}
            />
          <Icon
            disabled={this.state.copyDisabled}
            color='#3ab'
            reverse
            name='camera'
            onPress={()=>this.pickImage(true)}
            />
          <Icon
            disabled={this.state.copyDisabled}
            color='#3ab'
            reverse
            name='attachment'
            onPress={()=>this.pickFile()}
            />
          <Icon
            disabled={this.state.selectedAttachments.length==0}
            color='#3ab'
            reverse
            name='delete'
            onPress={()=>this.deleteAttachments()}
            />
        </View>
        <Collapsible
          beginCollapsed={true}
          titleStyle={styles.collapsibleHead}
          style={styles.collapsible}
          title={`Images >${this.props.device.images.length?' ...':''}`}>
        {
          this.props.device.images.concat(this.state.filesAsImages?this.props.device.files:[]).map((s,i)=>(
            <View key={i}>
              <Text>{s.name}</Text>
              {
                this.props.device.images.indexOf(s)==-1 && !s.type.match(/^image\//) ? (
                  <TouchableHighlight
                    key={i}
                    underlayColor='#aaa'
                    style={this.state.selectedAttachments.indexOf(s)!=-1?styles.selectedFile:styles.unselectedFile}
                    onPress={()=>this.state.selectedAttachments.length>0?this.selectAttachment(s):this.openFile(s)}
                    onLongPress={()=>this.selectAttachment(s)}>
                    <View style={{flex:1,flexDirection:'row'}}>
                      <Icon name='block'/>
                      <Text style={{marginLeft:10}}>{s.fileName}</Text>
                    </View>
                  </TouchableHighlight>
                ) : (
                  <TouchableHighlight
                    style={this.state.selectedAttachments.indexOf(s)!=-1?styles.selectedImage:styles.unselectedImage}
                    onPress={()=>this.state.selectedAttachments.length>0?this.selectAttachment(s):this.openImage(s)}
                    onLongPress={()=>this.selectAttachment(s)}
                    >
                    <View
                      style={this.state.selectedAttachments.indexOf(s)!=-1?{opacity:0.5}:{}}>
                    <FlexImage source={{uri:'file://'+s.localPath}}/>
                    </View>
                  </TouchableHighlight>
                )
              }
            </View>
          ))
        }
          <CheckBox
            checked={this.state.filesAsImages}
            title={'Include files'}
            onPress={()=>this.setState({filesAsImages:!this.state.filesAsImages})}/>
        </Collapsible>
        <Collapsible
          beginCollapsed={true}
          titleStyle={styles.collapsibleHead}
          style={styles.collapsible}
          title={`Files >${this.props.device.files.length?' ...':''}`}>
          {
            this.props.device.files.concat(this.state.imagesAsFiles?this.props.device.images:[]).map((f,i)=>(
              <TouchableHighlight
                key={i}
                underlayColor='#aaa'
                style={this.state.selectedAttachments.indexOf(f)!=-1?styles.selectedFile:styles.unselectedFile}
                onPress={()=>this.state.selectedAttachments.length>0?this.selectAttachment(f):this.openFile(f)}
                onLongPress={()=>this.selectAttachment(f)}>
                <View style={{flex:1,flexDirection:'row'}}>
                  <Icon name={this.props.device.images.indexOf(f)!=-1?'image':'attachment'}/>
                  <Text style={{marginLeft:10}}>{f.fileName}</Text>
                </View>
              </TouchableHighlight>
            ))
          }
          <CheckBox
            checked={this.state.imagesAsFiles}
            title={'Include images'}
            onPress={()=>this.setState({imagesAsFiles:!this.state.imagesAsFiles})}/>
        </Collapsible>
        {
          this.state.showPreview ? (
            <StatusBar hidden={true}/>
          ) : null
        }
        {
          this.state.preview ? (
            <Overlay visible={this.state.showPreview}
              onClose={()=>{
                this.setState({
                  showPreview:false, 
                  zoomPreview:1, 
                  zoomPreviewValue:new Animated.Value(1)})
              }}
              childrenWrapperStyle={{padding:0,margin:0,width:'100%',backgroundColor:'transparent'}}
              containerStyle={{padding:0}}
              animationDuration={250}
              closeOnTouchOutside>
              {
                (hideModal, overlayState)=>(
                    <TouchableHighlight
                      underlayColor='transparent'
                      style={{width:'100%',height:'100%',alignItems:'center',justifyContent:'center'}}
                      onPress={hideModal}
                      onLongPress={()=>{
                        this.setState({zoomPreview:this.state.zoomPreview<3?this.state.zoomPreview*=1.5:1},()=>{
                          Animated.timing(
                            this.state.zoomPreviewValue,
                            {
                              toValue: this.state.zoomPreview,
                              duration: 200
                            }
                          ).start()
                        });
                      }}>
                      <View style={{flex:1,height:'100%',width:'100%'}}>
                        <View style={{flex:1,justifyContent:'center'}}>
                          <Animated.View
                            style={{transform:[
                              {scaleX:this.state.zoomPreviewValue},
                              {scaleY:this.state.zoomPreviewValue}]}}
                            >
                            <FlexImage source={{uri:'file://'+this.state.preview.localPath}} style={{width:'100%'}}/>
                          </Animated.View>
                        </View>
                        <View style={{alignSelf:'flex-start',position:'absolute',width:'100%',padding:20,
                          flexDirection:'row',justifyContent:'space-between',alignItems:'center',backgroundColor:'rgba(0,0,0,0.8)'}}>
                          <Icon
                            size={40}
                            color='#fff'
                            name='arrow-back'
                            underlayColor='transparent'
                            />
                          <Text style={{color:'white',fontSize:18,width:'65%'}}>{this.state.preview.fileName.length>27?(this.state.preview.fileName.substr(0,24)+'...'):this.state.preview.fileName.length}</Text>
                          <Icon
                            size={40}
                            color='#fff'
                            name='delete'
                            underlayColor='transparent'
                            onPress={()=>{
                              hideModal();
                              this.setState({selectedAttachments:[this.state.preview]},()=>{
                                this.deleteAttachments();
                              });
                            }}
                            />
                          <Icon
                            size={40}
                            color='#fff'
                            name='edit'
                            underlayColor='transparent'
                            onPress={()=>{
                              this.openFile(this.state.preview)
                              hideModal();
                            }}
                            />
                        </View>
                      </View>
                    </TouchableHighlight>
                )
              }
            </Overlay>
          ) : null
        }
      </Collapsible>
    )
  }
}
const styles = StyleSheet.create({
  selectedImage: {
    backgroundColor:'#0ab',
    borderRadius:5,
    opacity:0.8,
    overflow:'hidden'
  },
  unselectedImage: {
    borderRadius:5,
    overflow:'hidden'
  },
  selectedFile: {
    backgroundColor:'#5bc',
    padding:20,
    borderRadius:5,
    overflow:'hidden',
    marginVertical:5,
  },
  unselectedFile: {
    backgroundColor:'#eee',
    padding:20,
    borderRadius:5,
    overflow:'hidden',
    marginVertical:5,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'white'
  },
   inputrow: {
    flexDirection: 'row',
    paddingHorizontal:30,
    paddingBottom: 15
  },
  inputbutton: {
    alignItems: 'center',
    backgroundColor: '#2c3539',
    paddingVertical: 10,
    paddingHorizontal: 30,
    marginTop:16
  },
  inputinput: {
    flex:1,
    marginTop:16,
    marginRight:20
  },
  camButton: {
    position: 'relative',
    alignItems: 'center',
    padding: 10,
    width:200,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#2c3539',
    padding: 10,
    width:300,
    marginTop:16
  },
  heading: { 
    color: 'black', 
    fontSize: 24, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 30 
  },
  simpleText: { 
    color: 'black', 
    fontSize: 20, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 16
  },
  collapsible:{
    borderLeftWidth:1,
    paddingLeft:10,
    borderColor:'#aaa',
    paddingBottom:15
  },
  collapsibleHead: { 
    color: '#777', 
    fontSize: 20,  
    padding: 16
  }
});