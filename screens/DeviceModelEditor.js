import React, { Component } from 'react';
import { FlatList, ScrollView, BackHandler, Alert, Text, View, Linking, TouchableHighlight, PermissionsAndroid, Platform, StyleSheet} from 'react-native';
import { Slider, Button, Input, ListItem } from 'react-native-elements';
import { DeviceSectionEditor } from './DeviceSectionEditor'
import { DevicePortGroup } from './DevicePortGroup'
import { Collapsible } from './Collapsible'
import { ColorPicker } from 'react-native-color-picker'


export class DeviceModelEditor extends Component {
  constructor() {
    super();
    this.state = {
      isDeviceEditorCollapsed: true
    };
  }
  updateSelf(self) {
    for (var key in self)
      this.props.device[key]=self[key];
    this.props.onEdit();
  }
  collapseToggle(key) {
    var newState={};
    newState[key]=!this.state[key];
    this.setState(newState);
  }
  componentWillMount() {
    /*this.backHandler = BackHandler.addEventListener('hardwareBackPress', ()=>{
      this.props.callback(null); 
      return true;});*/
  }
  componentWillUnmount() {
    /*this.backHandler.remove();*/
  }
  
  render() {
    let displayModal;
    //If qrvalue is set then return this view
    return (
      <View>
        <Collapsible
          style={styles.collapsible}
          titleStyle={styles.collapsibleHead}
          buttonStyle={{padding:16}}
          title="Edit device shape >">
          <Text>Device size: {this.props.device.U}U</Text>
          <Slider
            step={1} minimumValue={0} maximumValue={4} value={this.props.device.U}
            onValueChange={(value) => this.updateSelf({U:value})}/>
          <Text>Device width: {this.props.device.width} rack</Text>
          <Slider
            step={0.25} minimumValue={0} maximumValue={1} value={this.props.device.width}
            onValueChange={(value) => this.updateSelf({width:value})}/>
          <ColorPicker
            style={{height:300}}
            oldColor={this.props.device.color}
            onColorSelected={(color)=>this.updateSelf({color:color})}/>
          <DeviceSectionEditor
            index={[]}
            self={this.props.device}
            level={1}
            onEdit={()=>this.props.onEdit()}
            />
        </Collapsible>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'white'
  },
   inputrow: {
    flexDirection: 'row',
    paddingHorizontal:30,
    paddingBottom: 15
  },
  inputbutton: {
    alignItems: 'center',
    backgroundColor: '#2c3539',
    paddingVertical: 10,
    paddingHorizontal: 30,
    marginTop:16
  },
  inputinput: {
    flex:1,
    marginTop:16,
    marginRight:20
  },
  camButton: {
    position: 'relative',
    alignItems: 'center',
    padding: 10,
    width:200,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#2c3539',
    padding: 10,
    width:300,
    marginTop:16
  },
  heading: { 
    color: 'black', 
    fontSize: 24, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 30 
  },
  simpleText: { 
    color: 'black', 
    fontSize: 20, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 16
  },
  collapsible:{
    borderLeftWidth:1,
    paddingLeft:10,
    borderColor:'#aaa',
    paddingBottom:15
  },
  collapsibleHead: { 
    color: '#777', 
    fontSize: 20,  
    padding: 16
  }
});