import { Alert } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import CryptoJS from 'crypto-js'

const CryptoJSAesJson = {
    stringify: function (cipherParams) {
      var j = {ct: cipherParams.ciphertext.toString(CryptoJS.enc.Base64)};
      if (cipherParams.iv) j.iv = cipherParams.iv.toString();
      if (cipherParams.salt) j.s = cipherParams.salt.toString();
      return JSON.stringify(j);
    },
    parse: function (jsonStr) {
      var j = JSON.parse(jsonStr);
      var cipherParams = CryptoJS.lib.CipherParams.create({ciphertext: CryptoJS.enc.Base64.parse(j.ct)});
      if (j.iv) cipherParams.iv = CryptoJS.enc.Hex.parse(j.iv);
      if (j.s) cipherParams.salt = CryptoJS.enc.Hex.parse(j.s);
      return cipherParams;
    }
}

export class DataMgr {
  constructor() {
  }
  static decAES(privateKey, data){
    return CryptoJS.AES.decrypt(data, privateKey, {format: CryptoJSAesJson}).toString(CryptoJS.enc.Utf8);
  }
  static encAES(privateKey, data){
    return CryptoJS.AES.encrypt(data, privateKey, {format: CryptoJSAesJson}).toString();
  }

  static clear(){
    AsyncStorage.clear();
  }

  static loadAsyncStorage(passphrase, key, msg1, msg2, def, cb){
    var that = this;
    async function load(){
      try {
        const ret = await AsyncStorage.getItem(key);
        if (ret === null){
          Alert.alert(
            msg1,
            msg2,
            [
              {text: 'Yeh', onPress: async () => {
                var encData = DataMgr.encAES(passphrase, JSON.stringify(def));
                await AsyncStorage.setItem(key, encData);
                cb(JSON.parse(JSON.stringify(def)));
              }},
              {text: 'Nuh-uh', onPress: () => {
                cb(null);
              }}
            ],
            {cancelable:false}
          )
        } else {
          try{
            var decData = DataMgr.decAES(passphrase,ret);
            cb(JSON.parse(decData));
          } catch {
            cb(null);
          }
        }
      } catch (err) {
        alert("Storage read error "+err);
        console.warn(err);
        cb(null);
      }
    }
    load();
  }
  static storeAsyncStorage(passphrase, key, data, cb){
    async function store(){
      try {
        var encData = DataMgr.encAES(passphrase, JSON.stringify(data));
        await AsyncStorage.setItem(key, encData);
        cb(true);
      } catch (err) {
        alert("Storage write error "+err);
        console.warn(err);
        cb(null);
      }
    }
    store();
  }
  static removeAsyncStorage(key, cb){
    async function remove(){
      try {
        await AsyncStorage.removeItem(key);
        cb(true);
      } catch (err) {
        alert("Storage write error "+err);
        console.warn(err);
        cb(null);
      }
    }
    remove();
  }
}