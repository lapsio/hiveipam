import React, { Component } from 'react';
import { ScrollView, BackHandler, Alert, Text, View, Linking, TouchableHighlight, PermissionsAndroid, Platform, StyleSheet} from 'react-native';
import { ListItem, Input, Button } from 'react-native-elements';
import { Collapsible } from './Collapsible'
import { InputRow } from './InputRow'

const clearUser = {
  name:'Bob',
  password:'',
  notes:''
} 

export class DeviceListPropEditor extends Component {
  constructor() {
    super();
    this.state = {
      //variable to hold the qr value
      filter: '',
    };
  }
  addUser(user){
    var u = this.props.device.userinfo.filter(u=>u.name == user)[0]||null;
    if (u)
      alert("User already exists");
    else {
      var newUser = JSON.parse(JSON.stringify(clearUser));
      newUser.name = user;
      this.props.device.userinfo.push(newUser);
      this.props.onEdit();
    }
  }
  editUser(user){
    this.props.onEdit();
  }
  deleteUser(user){
    var i = this.props.device.userinfo.indexOf(user);
    if (i != -1)
      this.props.device.userinfo.splice(i,1);
    this.props.onEdit();
  }
  componentWillMount() {
    /*this.backHandler = BackHandler.addEventListener('hardwareBackPress', ()=>{
      this.props.onSelect(null); 
      return true;});*/
  }
  componentWillUnmount() {
    /*this.backHandler.remove();*/
  }
  
  render() {
    let displayModal;
    //If qrvalue is set then return this view
    return (
      <View>
        {
          this.props.list
            .filter(el=>el.indexOf(this.state.filter)!=-1)
            .map((el, i)=>(
              <ListItem
                titleStyle={{fontSize:18}}
                containerStyle={{padding:5}}
                key={i}
                title={el}
                rightElement={
                  this.props.allowDelete?(
                    <Button  
                      type='clear' 
                      icon={{name:'clear'}}
                      onPress={() => this.props.onDelete(el)}/>
                  ) : null
                }/>
            ))
        }
        <InputRow
          placeholder={this.props.placeholder}
          buttonIcon='add'
          onChangeText={(text)=>this.setState({filter:text})}
          onButtonPress={(text)=>this.props.onAdd(text)}
          />
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'white'
  },
  inputrow: {
    flexDirection: 'row',
    paddingHorizontal:30,
    paddingBottom: 15
  },
  inputbutton: {
    alignItems: 'center',
    backgroundColor: '#2c3539',
    paddingVertical: 10,
    paddingHorizontal: 30,
    marginTop:16
  },
  inputinput: {
    flex:1,
    marginTop:16,
    marginRight:20
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#2c3539',
    padding: 10,
    width:300,
    marginTop:16
  },
  heading: { 
    color: 'black', 
    fontSize: 24, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 30 
  },
  simpleText: { 
    color: 'black', 
    fontSize: 20, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 16
  },
  collapsible:{
    borderLeftWidth:1,
    paddingLeft:10,
    borderColor:'#aaa'
  },
  collapsibleHead: { 
    color: '#777', 
    fontSize: 20,  
    padding: 16
  }
});