import React, { Component } from 'react';
import { BackHandler, Alert, Text, View, Linking, TouchableHighlight, PermissionsAndroid, Platform, StyleSheet} from 'react-native';
import { Icon, Input, Button } from 'react-native-elements';

export class InputRow extends Component {
  constructor() {
    super();
    this.state = {
      text:''
    };
  }
  componentWillMount(){
    //if ('beginCollapsed' in this.props)
      //this.setState({isCollapsed:this.props.beginCollapsed});
  }
  render() {
    return (
      <View style={styles.inputrow}>
        <Input 
          ref={(r) => this.input = r}
          defaultValue={this.props.defaultValue||''}
          secureTextEntry={this.props.secureTextEntry||false}
          containerStyle={styles.inputinput}
          placeholder={this.props.placeholder}
          onChangeText={(text) => {
            this.setState({text:text},()=>{
              if (this.props.onChangeText)
                this.props.onChangeText(text);
            })
          }}/>
        {
          this.props.buttonText ? (
            <Button
              disabled={this.props.buttonDisabled||false}
              buttonStyle={styles.inputbutton}
              title="Add"
              icon={'buttonIcon' in this.props ? {name:this.props.buttonIcon,color:'white'} : null}
              onPress={() => {
                this.props.onButtonPress(this.state.text)
                this.input.input.clear();
                if (this.props.onChangeText) this.props.onChangeText('');
              }}/>
          ) : this.props.buttonIcon ? (
            <Icon
              disabled={this.props.buttonDisabled||false}
              reverse
              color='#3ab'
              name={this.props.buttonIcon}
              onPress={()=>{
                this.props.onButtonPress(this.state.text)
                if (!this.props.noFlush){
                  this.input.input.clear();
                  if (this.props.onChangeText) this.props.onChangeText('');
                }
              }}
              />
          ) : null
        }
      </View>
    );
  }
} 

const styles = StyleSheet.create({
   inputrow: {
    flexDirection: 'row',
    paddingHorizontal:30,
    paddingBottom: 15
  },
  inputbutton: {
    alignItems: 'center',
    backgroundColor: '#2c3539',
    paddingVertical: 10,
    paddingHorizontal: 30,
    marginTop:16
  },
  inputinput: {
    flex:1,
    marginTop:16,
    marginRight:20
  }
});