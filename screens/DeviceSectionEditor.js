import React, { Component } from 'react';
import { FlatList, ScrollView, BackHandler, Alert, Text, View, Linking, TouchableHighlight, PermissionsAndroid, Platform, StyleSheet} from 'react-native';
import { CheckBox, ButtonGroup, Slider, Button, Input, ListItem } from 'react-native-elements';
import { DevicePortGroupEditor } from './DevicePortGroupEditor'
import { Collapsible } from './Collapsible'

const types={
  'section':0,
  'portgroup':1
}

const revTypes=[
  'section', 
  'portgroup'
];

const clearSection={
  type:'section',
  size:1,
  direction:'row',
  content:[]
}

const clearPortGroup={
  type:'portgroup',
  size:1,
  portdefaults:{
    medium:'sfp',
    type:'data'
  },
  flip:'none',
  direction:'column',
  width:4,
  height:2,
  gid:'1',
  sid:1
}

const directions={
  'row':0,
  'column':1,
}

const revDirections=[
  'row',
  'column',
]

const mediums={
  'copper':0,
  'sfp':1,
  'serial':2,
  'aux':3
}

const revMediums=[
  'copper',
  'sfp',
  'serial',
  'aux'
]


const portTypes={
  'mgmt':0,
  'aux':1,
  'data':2
}

const revPortTypes=[
  'mgmt',
  'aux',
  'data'
]

export class DeviceSectionEditor extends Component {
  constructor() {
    super();
    this.state = {
      isCollapsed:true
    };
  }
  collapse(c) {
    this.setState({isCollapsed:c})
  }
  updateChildren(cnum) {
    var sliced = this.props.self.content.slice(0,cnum);
    for (var i = sliced.length ; i < cnum ; ++i)
      sliced.push(JSON.parse(JSON.stringify(clearSection)));
    this.props.self.content = sliced;
    this.props.onEdit();
  }
  updateSelf(self) {
    for (var key in self)
      this.props.self[key]=self[key];
    this.props.onEdit();
  }
  morph(target) {
    var cSrc = this.props.self.type == 'portgroup'?clearPortGroup:clearSection,
        cTarget = target == 'portgroup'?clearPortGroup:clearSection,
        cloneTarget = JSON.parse(JSON.stringify(cTarget));
    if (cTarget == cSrc)
      return;

    for (var key in cSrc)
      if (!(key in cTarget))
        delete (this.props.self[key]);
    for (var key in cTarget)
      if (!(key in this.props.self))
        this.props.self[key] = cloneTarget[key];
    this.props.self.type = cloneTarget.type;
    this.props.onEdit();
  }
  flip(flip) {
    var newFlip;
    switch(this.props.self.flip){
      case flip:
        newFlip='none';
        break;
      case 'both':
        newFlip=(flip=='vertically')?'horizontally':'vertically';
        break;
      case 'none':
        newFlip=(flip=='vertically')?'vertically':'horizontally';
        break;
      case 'horizontally':
        newFlip=(flip=='vertically')?'both':'none';
        break;
      case 'vertically':
        newFlip=(flip=='vertically')?'none':'both';
        break;
    }
    this.updateSelf({flip:newFlip});
  }
  containsPortGroup(self) {
    return (self.type == 'portgroup' || self.content.filter(s=>this.containsPortGroup(s)).length>0);
  }
  componentWillMount() {
    /*this.backHandler = BackHandler.addEventListener('hardwareBackPress', ()=>{
      this.props.callback(null); 
      return true;});*/
    var def = null;
    if (this.props.self.type=='section')
      def = clearSection;
    if (this.props.self.type=='portgroup')
      def = clearPortGroup;

    if (def){
      for (var key in def)
        if (!(key in this.props.self))
          this.props.self[key]=JSON.parse(JSON.stringify(def[key]));
    
      if (this.props.self.portdefaults && def.portdefaults)
        for (var key in def.portdefaults)
          if (!(key in this.props.self.portdefaults)){ //TODO I'm not sure if it's necessary
            this.props.self.portdefaults={...this.props.self.portdefaults};
            this.props.self.portdefaults[key]=JSON.parse(JSON.stringify(def.portdefaults[key]))
          }
    }
  }
  componentWillUnmount() {
    /*this.backHandler.remove();*/
  }
  
  render() {
    let displayModal;
    //If qrvalue is set then return this view
    return (
      <View
        style={{
          
        }}>
        <Collapsible
          style={styles.collapsible}
          titleStyle={styles.collapsibleHead}
          buttonStyle={{padding:16}}
          title={`${this.props.index.join(':')} ${this.props.self.type=='portgroup'?': Port Group':(!this.props.self.type)?'Layout':': Section'} >${this.containsPortGroup(this.props.self)?'>':''}`}>
              {
                !this.props.self.code ? (  //root node
                  <React.Fragment>
                    <ButtonGroup
                      selectedIndex={types[this.props.self.type]}
                      buttons={revTypes}
                      onPress={(i)=>this.morph(revTypes[i])}/>
                    <ButtonGroup
                      selectedIndex={directions[this.props.self.direction]}
                      buttons={revDirections}
                      onPress={(i)=>this.updateSelf({direction:revDirections[i]})}/>
                    <Text>Size ratio: {this.props.self.size}</Text>
                    <Slider
                      step={1} minimumValue={1} maximumValue={10} value={this.props.self.size}
                      onValueChange={(value) => this.updateSelf({size: value})}/>
                  </React.Fragment>
                ) : null
              }
              {
                this.props.self.type=='portgroup' ? (
                  <React.Fragment>
                    <Text>Height (No. ports): {this.props.self.height}</Text>
                    <Slider
                      step={1} minimumValue={1} maximumValue={8} value={this.props.self.height}
                      onValueChange={(value) => this.updateSelf({height: value})}/>
                    <Text>Width (No. ports): {this.props.self.width}</Text>
                    <Slider
                      step={1} minimumValue={1} maximumValue={12} value={this.props.self.width}
                      onValueChange={(value) => this.updateSelf({width: value})}/>
                    <Text>Port group name: {this.props.self.gid}</Text>
                    <Input
                      defaultValue={String(this.props.self.gid)}
                      onChangeText={(value) => this.updateSelf({gid: value})}/>
                    <Text>First port number: {this.props.self.sid}</Text>
                    <Input
                      defaultValue={String(this.props.self.sid)}
                      keyboardType='number-pad'
                      onChangeText={(value) => (!isNaN(Number(value)))?this.updateSelf({sid: Number(value)}):''}/>
                    <CheckBox
                      title="Flip vertically"
                      checked={['vertically','both'].indexOf(this.props.self.flip) != -1}
                      onPress={()=>this.flip('vertically')}
                      />
                    <CheckBox
                      title="Flip horizontally"
                      checked={['horizontally','both'].indexOf(this.props.self.flip) != -1}
                      onPress={()=>this.flip('horizontally')}
                      />
                    <Text>Port group medium: {this.props.self.portdefaults.medium}</Text>
                    <ButtonGroup
                      selectedIndex={mediums[this.props.self.portdefaults.medium]}
                      buttons={revMediums}
                      onPress={(i)=>this.updateSelf({portdefaults:{...this.props.self.portdefaults, medium:revMediums[i]}})}/>
                    <Text>Port group type: {this.props.self.portdefaults.type}</Text>
                    <ButtonGroup
                      selectedIndex={portTypes[this.props.self.portdefaults.type]}
                      buttons={revPortTypes}
                      onPress={(i)=>this.updateSelf({portdefaults:{...this.props.self.portdefaults, type:revPortTypes[i]}})}/>
                  </React.Fragment>
                ) : (
                  <React.Fragment>
                  {
                    !this.props.self.code ? (
                      <React.Fragment>
                      </React.Fragment>
                    ) : null
                  }
                    <Text>SubSections: {this.props.self.content.length}</Text>
                    <Slider
                      step={1} minimumValue={0} maximumValue={10} value={this.props.self.content.length}
                      onValueChange={(value) => this.updateChildren(value)}/>
                  </React.Fragment>
                )
              }
              {
                this.props.self.type != 'portgroup' ? (
                  this.props.self.content.map((c,i)=>(
                      <DeviceSectionEditor
                        key={i}
                        index={this.props.index.concat([i])}
                        self={c}
                        level={this.props.level+1}
                        onEdit={this.props.onEdit}
                        />
                  ))
                ) : null
              }
        </Collapsible>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'white'
  },
   inputrow: {
    flexDirection: 'row',
    paddingHorizontal:30,
    paddingBottom: 15
  },
  inputbutton: {
    alignItems: 'center',
    backgroundColor: '#2c3539',
    paddingVertical: 10,
    paddingHorizontal: 30,
    marginTop:16
  },
  inputinput: {
    flex:1,
    marginTop:16,
    marginRight:20
  },
  camButton: {
    position: 'relative',
    alignItems: 'center',
    padding: 10,
    width:200,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#2c3539',
    padding: 10,
    width:300,
    marginTop:16
  },
  heading: { 
    color: 'black', 
    fontSize: 24, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 30 
  },
  simpleText: { 
    color: 'black', 
    fontSize: 20, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 16
  },
  collapsible:{
    borderLeftWidth:1,
    paddingLeft:10,
    borderColor:'#aaa',
    paddingBottom:15
  },
  collapsibleHead: { 
    color: '#777', 
    fontSize: 20,  
    padding: 16
  }
});