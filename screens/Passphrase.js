import React, { Component } from 'react';
import { BackHandler, Alert, Text, View, Linking, TouchableHighlight, PermissionsAndroid, Platform, StyleSheet} from 'react-native';
import { Header, Button, Input } from 'react-native-elements';
import { InputRow } from './InputRow'

export class Passphrase extends Component {
  constructor() {
    super();
    this.state = {
      passphrase:''
    };
  }
  componentWillMount() {
    /*this.backHandler = BackHandler.addEventListener('hardwareBackPress', ()=>{
      this.props.callback(null); 
      return true;});*/
  }
  componentWillUnmount() {
    /*this.backHandler.remove();*/
  }
  
  render() {
    let displayModal;
    //If qrvalue is set then return this view
    return (
      <View style={{flex:1}}>
        <Header
          statusBarProps={{backgroundColor:'#089',translucent:true}}
          containerStyle={{backgroundColor:'#3ab'}}
          placement="left"
          centerComponent={{text:"HiveIPAM", style:{fontSize:20,color:'white'}}}
          />
        <View style={styles.container}>
          <Text style={styles.heading}>Please provide passphrase</Text>
          <InputRow
            buttonIcon='check'
            placeholder="Passphrase"
            secureTextEntry={true}
            onButtonPress={(text)=>{this.props.onInput(text)}}/>
        </View>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'white'
  },
  inputrow: {
    flexDirection: 'row',
    paddingHorizontal:30
  },
  inputbutton: {
    alignItems: 'center',
    backgroundColor: '#2c3539',
    paddingVertical: 10,
    paddingHorizontal: 30,
    marginTop:16
  },
  inputinput: {
    flex:1,
    marginTop:16,
    marginRight:20
  },
  camButton: {
    position: 'relative',
    alignItems: 'center',
    padding: 10,
    width:200,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#2c3539',
    padding: 10,
    width:300,
    marginTop:16
  },
  heading: { 
    color: 'black', 
    fontSize: 24, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 30 
  },
  simpleText: { 
    color: 'black', 
    fontSize: 20, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 16
  }
});