import React, { Component } from 'react';
import { ScrollView, BackHandler, Alert, Text, View, Linking, TouchableHighlight, PermissionsAndroid, Platform, StyleSheet} from 'react-native';
import { Input, Header, Button } from 'react-native-elements';
import { DeviceModel } from './DeviceModel';
import { InventoryList } from './InventoryList';
import { DeviceModelEditor } from './DeviceModelEditor';
import { DeviceDetailsEditor } from './DeviceDetailsEditor';
import { DeviceAttachmentsEditor } from './DeviceAttachmentsEditor'
import { DevicePortsEditor } from './DevicePortsEditor'

const cleardev = {
  code:null,
  name:"Unknown Device",
  U:1,
  width:1,
  files:[],
  images:[],
  notes:'',
  color:'#eee',
  location:{
    rack:'',
    U:'',
    notes:''
  },
  userinfo:[],
  portinfo:{
  },
  content:[]
};

export class Editor extends Component {
  constructor() {
    super();
    this.state = {
      //variable to hold the qr value
      clone: false,
      create: false,
      device: null
    };
  }
  cloneDevice(code) {
    if (code != null){
      var item = this.props.inventory.hwList.filter(hw => hw.code == code)[0]||null,
          index = this.props.inventory.hwList.indexOf(item);
      if (index != -1){  //exists
        var newDevice = this.fixDevice(JSON.parse(JSON.stringify(item)));
        newDevice.code = this.props.code;
        this.setState({device:newDevice, clone:false})
      } else
        console.warn('Error during clone? - not found');
    } else
      this.onDone(null);
  }
  fixDevice(device){
    for (var key in cleardev)
      if (!(key in device))
        device[key]=JSON.parse(JSON.stringify(cleardev[key]));
    return device;
  }
  componentWillMount() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', ()=>{
      this.props.onDone(null); 
      return true;});
    if (this.props.device != null)
      this.setState({create: false, 
        device: this.fixDevice(JSON.parse(JSON.stringify(this.props.device)))});
    else if (this.props.inventory.hwList.length)
      Alert.alert(
        "Device editor",
        "Do you want to clone existing device?",
        [
          {text: 'Yeh', onPress: () => {
            this.setState({clone: true});
          }},
          {text: 'Nuh-uh', onPress: () => {
            var newDevice = JSON.parse(JSON.stringify(cleardev));
            newDevice.code = this.props.code;
            this.setState({device: newDevice});
          }}
        ]
      );
    else {
      var newDevice = JSON.parse(JSON.stringify(cleardev));
      newDevice.code = this.props.code;
      this.setState({device: newDevice});
    }
  }
  componentWillUnmount() {
    this.backHandler.remove();
  }
  
  render() {
    let displayModal;
    //If qrvalue is set then return this view
    if (this.state.clone)
      return (
        <InventoryList
          title='Clone device'
          inventory={this.props.inventory}
          allowNew={false}
          allowDelete={false}
          onDelete={(code)=>this.deleteItem(code)}
          onSelect={(code)=>{
            if (code == null)
              this.props.onDone(null);
            else
              this.cloneDevice(code);
          }}
          />
      )
    else if (this.state.device == null)
      return (
        <View style={styles.container}>
          <Text style={styles.heading}>Creating device...</Text>
        </View>
      )
    else
      return (
        <View style={{flex:1}}>
          <Header
            statusBarProps={{backgroundColor:'#089',translucent:true}}
            containerStyle={{backgroundColor:'#3ab'}}
            placement="left"
            leftComponent={{ icon: 'arrow-back', color: '#fff', underlayColor:'transparent', onPress:()=>this.props.onDone(null)}}
            centerComponent={{text:`${this.state.device.name}`, style:{fontSize:20,color:'white'}}}
            rightComponent={{ icon: 'save', color:'#fff', underlayColor:'transparent', onPress:()=>this.props.onDone(this.state.device)}}
            />
          <View style={styles.container}>
            <View style={{flexDirection:'row', flex:1}}>
              <View style={styles.previewPane}>
                <DeviceModel
                  device={this.state.device}/>
              </View>
              <ScrollView style={{
                padding:20,
                paddingLeft:10,
                paddingTop:10
              }}>
                <View style={styles.portsPane}>
                  <Text>Device code: {this.state.device.code}</Text>
                  <Input
                    placeholder="Device name"
                    defaultValue={this.state.device.name}
                    onChangeText={(text) => {this.state.device.name=text}}
                    />
                  <DeviceModelEditor
                    device={this.state.device}
                    onEdit={()=>this.setState({device: this.state.device})}
                  />
                  <DeviceDetailsEditor
                    device={this.state.device}
                    onEdit={()=>this.setState({device: this.state.device})}/>
                  <DevicePortsEditor
                    device={this.state.device}
                    onEdit={()=>this.setState({device: this.state.device})}/>
                  <DeviceAttachmentsEditor
                    realmName={this.props.realmName}
                    device={this.state.device}
                    onEdit={()=>this.setState({device: this.state.device})}/>
                </View>
              </ScrollView>
            </View>
          </View>
        </View>
      )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'white',
    overflow:'hidden'
  },
  previewPane: {
    minHeight:500,
    minWidth:150,
    alignItems:'center',
    justifyContent:'center'
  },
  portsPane: {
    flex: 1
  },
  detailsPane: {
    minHeight:200,
    backgroundColor:'#aff'
  },
  camButton: {
    position: 'relative',
    alignItems: 'center',
    padding: 10,
    width:200,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#2c3539',
    padding: 10,
    width:300,
    marginTop:16
  },
  heading: { 
    color: 'black', 
    fontSize: 24, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 30 
  },
  simpleText: { 
    color: 'black', 
    fontSize: 20, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 16
  }
});