import React, { Component } from 'react';
import { ScrollView, BackHandler, Alert, Text, View, Linking, TouchableHighlight, PermissionsAndroid, Platform, StyleSheet} from 'react-native';
import { Icon, Header, ListItem, Input, Button } from 'react-native-elements';
import { InputRow } from './InputRow'

export class InventoryList extends Component {
  constructor() {
    super();
    this.state = {
      //variable to hold the qr value
      filter: '',
    };
  }
  filterPass(hw){
    var fields = [hw.code, hw.name, hw.notes]
    Object.keys(hw.portinfo)
      .forEach(g=>Object.keys(hw.portinfo[g])
        .forEach(p=>fields=fields.concat(hw.portinfo[g][p].ips||[])));
    return (fields
            .map(f=>f.toLowerCase())
            .filter(f=>f.indexOf(this.state.filter.toLowerCase()) != -1)
            .length > 0);
  }
  componentWillMount() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', ()=>{
      this.props.onSelect(null); 
      return true;});
  }
  componentWillUnmount() {
    this.backHandler.remove();
  }
  
  render() {
    let displayModal;
    //If qrvalue is set then return this view
    return (
      <View style={{flex:1}}>
        <Header
          statusBarProps={{backgroundColor:'#089',translucent:true}}
          containerStyle={{backgroundColor:'#3ab'}}
          placement="left"
          leftComponent={{ icon: 'arrow-back', color: '#fff', underlayColor:'transparent', onPress:()=>this.props.onSelect(null)}}
          centerComponent={{text:this.props.title, style:{fontSize:20,color:'white'}}}
          />
        <View style={styles.container}>
          <ScrollView style={{position:'relative', alignSelf:'stretch'}}>
          {
            this.props.inventory.hwList
              .filter(hw => this.filterPass(hw))
              .map((hw, i)=>(
                <ListItem
                  key={i}
                  title={`${hw.name} [${hw.code}]`}
                  onPress={() => {this.props.onSelect(hw.code)}}
                  rightElement={
                    this.props.allowDelete?(
                      <Button  
                        style={{flex:1}}
                        type='clear' 
                        icon={{name:'clear'}}
                        onPress={() => this.props.onDelete(hw.code)}/>
                    ) : null
                  }/>
              ))
          }
          </ScrollView>
          <InputRow
            buttonIcon={this.props.allowNew?'add':null}
            placeholder='Device code'
            onChangeText={(text)=>this.setState({filter:text})}
            onButtonPress={(text)=>this.props.onSelect(text)}
            />
        </View>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'white'
  },
  inputrow: {
    flexDirection: 'row',
    paddingHorizontal:30,
    paddingBottom: 15
  },
  inputbutton: {
    alignItems: 'center',
    backgroundColor: '#2c3539',
    paddingVertical: 10,
    paddingHorizontal: 30,
    marginTop:16
  },
  inputinput: {
    flex:1,
    marginTop:16,
    marginRight:20
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#2c3539',
    padding: 10,
    width:300,
    marginTop:16
  },
  heading: { 
    color: 'black', 
    fontSize: 24, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 30 
  },
  simpleText: { 
    color: 'black', 
    fontSize: 20, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 16
  }
});