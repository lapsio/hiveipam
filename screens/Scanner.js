import React, { Component } from 'react';
import { BackHandler, Text, View, Linking, TouchableHighlight, PermissionsAndroid, Platform, StyleSheet} from 'react-native';
import { CameraKitCameraScreen, } from 'react-native-camera-kit';
import { Button } from 'react-native-elements';
import { Misc } from './Misc'

export class Scanner extends Component {
  constructor() {
    super();
    this.state = {
      //variable to hold the qr value
      qrvalue: null,
      torch: false,
      permissionGranted: false
    };
  }
  getPermission() {
    Misc.getPermission([PermissionsAndroid.PERMISSIONS.CAMERA],
      (success)=>{
        if (success)
          this.setState({permissionGranted: true});
      });
  }
  componentWillMount() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', ()=>{
      this.props.onReadCode(null); 
      return true;});
    this.getPermission();
  }
  componentWillUnmount() {
    this.backHandler.remove();
    this.setState({ qrvalue: null });
  }
  onOpenlink() {
    //Function to open URL, If scanned 
    Linking.openURL(this.state.qrvalue);
    //Linking used to open the URL in any browser that you have installed
  }
  onToggleFlash() {
    this.setState({ torch: !this.state.torch })
    this.cameraScreen.camera.setFlashMode(this.state.torch?'torch':'off');
  }
  onBarcodeScan(qrvalue) {
    this.setState({ qrvalue: qrvalue });
  }
  onConfirm(value) {
    this.props.onReadCode(value);
  }
  render() {
    let displayModal;
    //If qrvalue is set then return this view
    if (!this.state.permissionGranted) {
      return (
        <View style={styles.container}>
            <Text style={styles.heading}>We need Camera permissions</Text>
            <Button
            buttonStyle={styles.button}
            onPress={() => this.getPermission()}
            title="OK"/>
        </View>
      );
    } else if (this.state.qrvalue) {
      return (
        <View style={styles.container}>
          <Text style={styles.heading}>We found following identifier:</Text>
          <Text style={styles.simpleText}>{this.state.qrvalue ? 'Looks fien?: '+this.state.qrvalue : ''}</Text>
          <Button
            buttonStyle={styles.button}
            onPress={() => this.onConfirm(this.state.qrvalue)}
            title="Yeh"/>
          <Button
            buttonStyle={styles.button}
            onPress={() => this.setState({ qrvalue: null })}
            title="Nuh-uh"/>
          {this.state.qrvalue.match(/^https?:\/\//) ? 
            <Button
              buttonStyle={styles.button}
              onPress={() => this.onOpenlink()}
              title="Open Link"/>
            : null
          }
        </View>
      );
    }
    return (
      <View style={{ flex: 1, position: 'relative' }}>
        <CameraKitCameraScreen
          style={{ flex: 1 }}
          ref={cs => this.cameraScreen = cs}
          cameraOptions={{flashMode: 'on'}}
          showFrame={true}
          //Show/hide scan frame
          scanBarcode={true}
          //Can restrict for the QR Code only
          laserColor={'blue'}
          //Color can be of your choice
          frameColor={'yellow'}
          //If frame is visible then frame color
          colorForScannerFrame={'black'}
          //Scanner Frame color
          offsetForScannerFrame = {60}
          heightForScannerFrame = {150}
          onReadCode={event =>
            this.onBarcodeScan(event.nativeEvent.codeStringValue)
          }
        />
        <View>
          <Button
            buttonStyle={{padding:20}}
            title="Toggle flash"
            type="clear"
            onPress={() => this.onToggleFlash()}></Button>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'white'
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#2c3539',
    padding: 10,
    width:300,
    marginTop:16
  },
  heading: { 
    color: 'black', 
    fontSize: 24, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 30 
  },
  simpleText: { 
    color: 'black', 
    fontSize: 20, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 16
  }
});