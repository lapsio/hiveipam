import React, { Component } from 'react';
import { BackHandler, Alert, Text, View, Linking, TouchableHighlight, PermissionsAndroid, Platform, StyleSheet} from 'react-native';
import { Icon, Button } from 'react-native-elements';

export class Collapsible extends Component {
  constructor() {
    super();
    this.state = {
      isCollapsed:true
    };
  }
  componentWillMount(){
    if ('beginCollapsed' in this.props)
      this.setState({isCollapsed:this.props.beginCollapsed});
  }
  render() {
    return (
      <View>
        <View
          style={{flexDirection:'row'}}>
          <TouchableHighlight
            style={{flex:1}} 
            underlayColor='#ddd'
            onPress={()=>this.setState({isCollapsed:!this.state.isCollapsed})}>
            <Text style={this.props.titleStyle||{}}>{this.props.title}</Text>
          </TouchableHighlight>
          {
            this.props.allowDelete ? (
              <Button 
                buttonStyle={this.props.buttonStyle||{flex:1}}
                titleStyle={{color:'#000', width:40}} 
                type='clear' 
                icon={{name:this.props.icon||'clear'}}
                onPress={() => this.props.onDelete()}/>
            ) : null
          }
        </View>
        {
          !('collapsed' in this.props?this.props.collapsed:this.state.isCollapsed) ? (
            <View style={this.props.style||{}}>
              {this.props.children}
            </View>
          ) : null
        }
      </View>
    );
  }
} 