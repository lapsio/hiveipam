import React, { Component } from 'react';
import { FlatList, ScrollView, BackHandler, Alert, Text, View, Linking, TouchableHighlight, PermissionsAndroid, Platform, StyleSheet} from 'react-native';
import { Slider, Button, Input, ListItem } from 'react-native-elements';
import { DeviceCredentialsEditor } from './DeviceCredentialsEditor';
import { Collapsible } from './Collapsible'

export class DeviceDetailsEditor extends Component {
  constructor() {
    super();
    this.state = {
      isDetailsEditorCollapsed: false
    };
  }
  updateSelf(self, silent) {
    for (var key in self)
      this.props.device[key]=self[key];
    if (!silent)
      this.props.onEdit();
  }
  collapseToggle(key) {
    var newState={};
    newState[key]=!this.state[key];
    this.setState(newState);
  }
  updateLocation(location, silent){
    for (var key in location)
      this.props.device.location[key]=location[key];
    if (!silent)
      this.props.onEdit();
  }
  parseLocation(){
    var Upos = Number(this.props.device.location.U);
    return (isNaN(Upos)?
      'unknown':
      this.props.device.U>1?
        `${Upos}-${Upos+this.props.device.U-1}`:
        Upos);
  }
  componentWillMount() {
    /*this.backHandler = BackHandler.addEventListener('hardwareBackPress', ()=>{
      this.props.callback(null); 
      return true;});*/
  }
  componentWillUnmount() {
    /*this.backHandler.remove();*/
  }
  
  render() {
    let displayModal;
    //If qrvalue is set then return this view
    return (
      <Collapsible
        beginCollapsed={false}
        style={styles.collapsible}
        titleStyle={styles.collapsibleHead}
        buttonStyle={{padding:16}}
        title="Edit device info >">
        <Collapsible
          beginCollapsed={false}
          style={styles.collapsible}
          titleStyle={styles.collapsibleHead}
          buttonStyle={{padding:16}}
          title="Location >">
          <Text>Rack id: {this.props.device.location.rack}</Text>
          <Input 
            defaultValue={this.props.device.location.rack}
            containerStyle={styles.inputinput}
            placeholder="Rack"
            onChangeText={(text) => {this.updateLocation({rack: text})}}/>
          <Text>Rack position: {this.parseLocation()}</Text>
          <Input 
            defaultValue={this.props.device.location.U}
            containerStyle={styles.inputinput}
            placeholder="Rack position"
            onChangeText={(text) => {this.updateLocation({U: text})}}/>
          <Text>Notes</Text>
          <Input
            placeholder="Notes"
            multiline={true}
            numberOfLines={2}
            defaultValue={this.props.device.location.notes}
            onChangeText={(text) => this.updateLocation({notes:text}, true)}/>
        </Collapsible>
        <Collapsible
          beginCollapsed={false}
          style={styles.collapsible}
          titleStyle={styles.collapsibleHead}
          buttonStyle={{padding:16}}
          title="Credentials >">
          <DeviceCredentialsEditor
            device={this.props.device}
            allowNew={true}
            onEdit={()=>{this.props.onEdit()}}
            />
        </Collapsible>
        <Collapsible
          style={styles.collapsible}
          titleStyle={styles.collapsibleHead}
          buttonStyle={{padding:16}}
          title={`Additional notes >${this.props.device.notes?' ...':''}`}>
          <Text>Notes</Text>
          <Input
            placeholder="Notes"
            multiline={true}
            numberOfLines={2}
            defaultValue={this.props.device.notes}
            onChangeText={(text) => this.updateSelf({notes:text}, true)}/>
        </Collapsible>
      </Collapsible>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'white'
  },
   inputrow: {
    flexDirection: 'row',
    paddingHorizontal:30,
    paddingBottom: 15
  },
  inputbutton: {
    alignItems: 'center',
    backgroundColor: '#2c3539',
    paddingVertical: 10,
    paddingHorizontal: 30,
    marginTop:16
  },
  inputinput: {
    flex:1,
    marginTop:16,
    marginRight:20
  },
  camButton: {
    position: 'relative',
    alignItems: 'center',
    padding: 10,
    width:200,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#2c3539',
    padding: 10,
    width:300,
    marginTop:16
  },
  heading: { 
    color: 'black', 
    fontSize: 24, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 30 
  },
  simpleText: { 
    color: 'black', 
    fontSize: 20, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 16
  },
  collapsible:{
    borderLeftWidth:1,
    paddingLeft:10,
    borderColor:'#aaa',
    paddingBottom:15
  },
  collapsibleHead: { 
    color: '#777', 
    fontSize: 20,  
    padding: 16
  }
});