import React, { Component } from 'react';
import { ScrollView, BackHandler, Alert, Text, View, Linking, TouchableHighlight, PermissionsAndroid, Platform, StyleSheet} from 'react-native';
import { Icon, ButtonGroup, Input, Header, Button } from 'react-native-elements';
import { InventoryList } from './InventoryList';
import { DataMgr } from './DataMgr'
import { InputRow } from './InputRow'
import { zip, unzip, subscribe } from 'react-native-zip-archive';
import FilePickerManager from 'react-native-file-picker';
import { Misc } from './Misc'
var RNFetchBlob = require('react-native-fetch-blob').default;
var RNFS = require('react-native-fs');

const importModes = {
  'local':0,
  'remote':1
}

const revImportModes = [
  'local',
  'remote'
]

export class ImportManager extends Component {
  constructor() {
    super();
    this.state = {
      //variable to hold the qr value
      passphrase: '',
      source:'',
      importMode:'local',
      gotData:false,
      loadComplete:true,
      loadSuccess:false,
      progress:'error',
      remoteFileName:'',
      remoteFiles:[]
    };
  }
  isSourceValid(){
    if (this.state.importMode == 'local')
      return this.state.source.match(/^\/.*\.hiv$/) && true;
    if (this.state.importMode == 'remote')
      return this.state.source.match(/^https?:\/\/.+/) && this.state.remoteFileName.match(/^[0-9]+-[0-9]+\.hiv$/) && true;
  }
  isAddressValid(){
    return this.state.source.match(/^https?:\/\/.+/) && true;
  }
  isRemoteFileValid(){
    return this.state.remoteFileName.match(/^[0-9]+-[0-9]+\.hiv$/) && true;
  }
  isPassphraseValid(){
    return this.state.passphrase.length >= 8
  }
  fetchFilesList(){
    var xhr = new XMLHttpRequest();
    xhr.open('GET', this.state.source+'/hivs');
    xhr.onload=()=>{
      if (xhr.status == 200){
        var files;
        try {
          files = JSON.parse(xhr.responseText).files;
        } catch (e) {
          Alert.alert(
            'Server error',
            'Cannot parse server response'
          )
        }
        this.setState({remoteFiles:files});
      }
      else
        Alert.alert(
          'Server error',
          'Network error: '+xhr.status
        );
    }
    xhr.send();
  }
  pickFile(){
    Misc.getPermission([
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.CAMERA
      ],(res)=>{
        if (res)
          FilePickerManager.showFilePicker((response)=>{
            if (response.error) 
              return Alert.alert(
                'Error',
                'Error during file read: '+response.error
              );
            if (response.didCancel || response.didRequestPermission || !response.uri) 
              return;
            
            this.setState({source:response.path});
          })
        else
          Alert.alert(
            'Notice',
            'Required permissions have not been granted'
          );
    })
  }
  loadBundle(cb){
     Misc.getPermission([
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE
      ],(res)=>{
        if (res){
          var srcPath = this.state.source;
          this.importBundle(srcPath,cb);
        } else
          cb(false,'Required permissions have not been granted')
      });
  }
  downloadBundle(cb){
    var srcPath = RNFS.DocumentDirectoryPath+'/download-bundle.hiv';
    RNFetchBlob.config({
        path:srcPath
      })
      .fetch('GET', this.state.source+'/hivs/'+this.state.remoteFileName, {})
      .progress((received,total)=>{
        this.setState({progress:`Download progress ${total==-1?'unknown':(100*received/total).toFixed(1)+'%'}`});
      })
      .then((res) => {
        if (res.respInfo.status == 200){
          this.importBundle(srcPath,cb);
        } else
          cb(false,'Network error: '+res.respInfo.status);
      })
      .catch((err)=>{
        cb(false,err);
      })
  }
  importBundle(srcPath, cb){
    var dstPath = RNFS.DocumentDirectoryPath+'/import-'+Date.now()+'-'+(~~(Math.random()*1000000000));
    
    RNFS.exists(srcPath)
      .then((success)=>{
        if (!success)
          cb(false,'Bundle not found');
        else {
          RNFS.mkdir(dstPath)
            .then((success)=>{
              this.setState({progress:'unpacking bundle'});
              unzip(srcPath,dstPath)
                .then((path)=>{
                  this.setState({progress:'decrypting device data'});
                  RNFS.readFile(dstPath+'/device.hivd','ascii')
                    .then((success)=>{
                      var decDev;
                      try{
                        decDev = JSON.parse(DataMgr.decAES(this.state.passphrase,success));
                      } catch (e) {
                        return cb(false,['Bundle decryption failed',e,'(passphrase incorrect or bundle corrupted)']);
                      }
                      var devPath = RNFS.DocumentDirectoryPath+'/attachments/'+this.props.realmName+'/'+decDev.code+'/';
                      (decDev.images||[]).forEach(i=>i.localPath=i.localPath.replace(/.*\/attachments\/.*\/.*\//,devPath));
                      (decDev.files||[]).forEach(f=>f.localPath=f.localPath.replace(/.*\/attachments\/.*\/.*\//,devPath));
                      RNFS.mkdir(devPath)
                        .then((success)=>{
                          RNFS.mkdir(dstPath+'/attachments/')
                            .then((success)=>{
                              this.copyDir(dstPath+'/attachments/',devPath,(success,err)=>{
                                if (!success)
                                  cb(false,err)
                                else {
                                  this.setState({progress:'cleanup'});
                                  this.deleteDir(dstPath+'/attachments/',(success,err)=>{
                                    if (!success)
                                      cb(false,err)
                                    else
                                      this.deleteDir(dstPath,(success,err)=>{
                                        cb(decDev);
                                      })
                                  })
                                }
                              })
                            }).catch((err)=>{
                              cb(false,err);
                            })
                        }).catch((err)=>{
                          cb(false,err);
                        })
                    }).catch((err)=>{
                      cb(false,err);
                    })
                }).catch((err)=>{
                  cb(false,err);
                })
            }).catch((err)=>{
              cb(false,err);
            })
        }
      }).catch((err)=>{
        cb(false,err);
      })
  }
  copyDir(src, dst, cb){
    RNFS.readdir(src)
      .then((success)=>{
        var cnt = success.length,
            errors = [],
            onDone = ()=>{
              this.setState({progress:`copying files ${success.length-cnt}/${success.length}`});
              if (cnt-- == 0){
                if (errors.length == 0)
                  cb(true);
                else
                  cb(false,errors);
              }
            }
        success.forEach(d=>{
          RNFS.copyFile(src+'/'+d, dst+'/'+d)
            .then((success)=>{
              onDone();
            }).catch((err)=>{
              errors.push(err);
              onDone();
            })
        })
        onDone();
      }).catch((err)=>{
        cb(false,err);
      })
  }
  deleteDir(dir, cb){
    RNFS.readdir(dir)
      .then((success)=>{
        var cnt = success.length,
            errors = [],
            onDone = ()=>{
              this.setState({progress:`cleanup ${success.length-cnt}/${success.length}`});
              if (cnt-- == 0){
                if (errors.length == 0)
                  RNFS.unlink(dir)
                    .then((success)=>{
                      cb(true);
                    }).catch((err)=>{
                      cb(false,[err]);
                    });
                else
                  cb(false,errors);
              }
            }
        success.forEach(d=>{
          RNFS.unlink(dir+'/'+d)
            .then((success)=>{
              onDone();
            }).catch((err)=>{
              errors.push(err);
              onDone();
            })
        })
        onDone();
      }).catch((err)=>{
        cb(false,err);
      })
  }
  cleanup(cb){
    var srcPath = RNFS.DocumentDirectoryPath;
    RNFS.readdir(RNFS.DocumentDirectoryPath)
      .then((success)=>{
        success = success.filter(d=>d.match(/^import-/));
        var cnt = success.length,
            errors = [],
            onDone = ()=>{
              if (cnt-- == 0){
                if (errors.length == 0)
                  cb(true,success.length);
                else
                  cb(false,errors);
              }
            }
        success.forEach(d=>{
          RNFS.exists(RNFS.DocumentDirectoryPath+'/'+d+'/attachments/')
            .then((exists)=>{
              if (exists)
                this.deleteDir(RNFS.DocumentDirectoryPath+'/'+d+'/attachments/',(success,err)=>{
                  if (success)
                    this.deleteDir(RNFS.DocumentDirectoryPath+'/'+d, (success,err)=>{
                      if (!success)
                        err.forEach(e=>errors.push(e));
                      onDone();
                    })
                  else {
                    err.forEach(e=>errors.push(e));
                    onDone();
                  }
                });
              else
                this.deleteDir(RNFS.DocumentDirectoryPath+'/'+d, (success,err)=>{
                  if (!success)
                    err.forEach(e=>errors.push(e));
                  onDone();
                });
            }).catch((err)=>{
              errors.push(err)
              onDone();
            })
        })
        onDone();
      }).catch((err)=>{
        console.warn('Error during cleanup');
      })
  }
  componentWillMount() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', ()=>{
      if (this.state.loadComplete)
        this.props.onDone(null); 
      return true;});
    this.cleanup((success,ret)=>{
      if (success && ret>0)
        Alert.alert(
          'Notice',
          `Cleaned up ${ret} rougue temporary files left after recent import crash`
        );
      else if (!success)
        Alert.alert(
          'Error',
          'Error occured during rougue temporary files cleanup:'+err.join('\n')
        );
    });
  }
  componentWillUnmount() {
    this.backHandler.remove();
  }
  render() {
    let displayModal;
    //If qrvalue is set then return this view
    if (this.state.gotData == false)
      return (
        <View style={{flex:1}}>
          <Header
            statusBarProps={{backgroundColor:'#089',translucent:true}}
            containerStyle={{backgroundColor:'#3ab'}}
            placement="left"
            leftComponent={{ icon: 'arrow-back', color: '#fff', underlayColor:'transparent', onPress:()=>this.props.onDone(null)}}
            centerComponent={{text:'Import settings', style:{fontSize:20,color:'white'}}}
            />
          <View style={styles.container}>
            <View style={{padding:50, flex:1, width:'100%'}}>
              <Text>Passphrase for bundle encryption</Text>
              <Input
                defaultValue={this.state.passphrase}
                placeholder="Passphrase"
                secureTextEntry={true}
                onChangeText={(text)=>this.setState({passphrase:text})}
                />
              {
                !this.isPassphraseValid() ? (
                  <Text style={styles.errorText}>Passphrase must be at least 8 characters long</Text>
                ) : null
              }
              <ButtonGroup
                selectedIndex={importModes[this.state.importMode]}
                buttons={revImportModes}
                onPress={(i)=>this.setState({importMode:revImportModes[i]})}/>
              <Text>{this.state.importMode == 'local'?'File name':'Server address'}</Text>
              {
                this.state.importMode == 'local' ? (
                  <View style={{alignSelf:'center'}}>
                    <Button 
                      buttonStyle={styles.button}
                      onPress={() => this.pickFile()}
                      title='Select bundle file'/>
                  </View>
                ) : (
                  <InputRow
                    buttonIcon='refresh'
                    buttonDisabled={!this.isAddressValid()}
                    defaultValue={this.state.source}
                    placeholder={this.state.importMode == 'local'?'File name':'Address'}
                    noFlush={true}
                    onChangeText={(text)=>this.setState({source:text,remoteFiles:[]})}
                    onButtonPress={()=>this.fetchFilesList()}
                    />
                )
              }
              {
                this.state.importMode == 'remote' && !this.isAddressValid() ? (
                  <Text style={styles.errorText}>Invalid server address (Note: protocol is required)</Text>
                ) : this.state.importMode == 'local' && !this.isSourceValid() ? (
                  <Text style={styles.errorText}>Illegal filename</Text>
                ) : null
              }
              {
                this.state.importMode == 'local' && this.isSourceValid() ? (
                  <React.Fragment>
                    <Text style={styles.simpleText}>Bundle will be loaded from:</Text>
                    <Text>{this.state.source}</Text>
                  </React.Fragment>
                ) : null
              }
              {
                this.state.importMode == 'remote' && this.isAddressValid() ? (
                  <React.Fragment>
                  {
                    this.state.remoteFiles.map((f,i)=>(
                      <View style={{alignSelf:'center'}} key={i}>
                        <Button 
                          buttonStyle={styles.button}
                          onPress={() => this.setState({remoteFileName:f})}
                          title={f}/>
                      </View>
                    ))
                  }
                    <Input
                      defaultValue={this.state.remoteFileName}
                      placeholder='Remote file name'
                      onChangeText={(text)=>this.setState({remoteFileName:text})}
                      />
                    {
                      !this.isRemoteFileValid() ? (
                        <Text style={styles.errorText}>Illegal remote file name</Text>
                      ) : null
                    }
                  </React.Fragment>
                ) : null
              }
              <View style={{position:'absolute',bottom:0,right:0,padding:20}}>
                <Icon
                  reverse
                  name={this.state.importMode=='local'?'move-to-inbox':'cloud-download'}
                  disabled={!this.isSourceValid() || !this.isPassphraseValid()}
                  onPress={()=>{
                    this.setState({gotData:true, progress:'preparing', loadSuccess:false, loadComplete:false}, ()=>{
                      var cb = (success,err)=>{
                        this.setState({progress:'done',loadComplete:true, loadSuccess:success?true:false, device:success?success:null});
                        if (success) {
                          var exdev = this.props.inventory.hwList.filter(d=>d.code == success.code)[0];
                          if (exdev)
                            Alert.alert(
                              'Conflict',
                              `Device with code: ${success.code} already exists in ${this.props.realmName} realm. Replace?`,
                              [
                                {text:'Yeh', onPress:()=>{
                                  var index = this.props.inventory.hwList.indexOf(exdev);
                                  this.props.inventory.hwList.splice(index,1,success);
                                  this.props.onImport(this.props.inventory);
                                }},
                                {text:'Nuh-uh',onPress:()=>this.setState({loadSuccess:false,device:null})}
                              ]
                            );
                          else {
                            this.props.inventory.hwList.push(success);
                            this.props.onImport(this.props.inventory);
                          }
                        } else {
                          this.cleanup((success,err2)=>{
                            Alert.alert(
                              'Error',
                              'Import finished with following error:\n'+(Array.isArray(err)?err.join('\n'):err)+(success?'':'\n\nTemporary files could not be cleaned up :C')
                            );
                          })
                        }
                      };
                      if (this.state.importMode == 'local')
                        this.loadBundle(cb);
                      else
                        this.downloadBundle(cb);
                    });
                  }}/>
              </View>
            </View>
          </View>
        </View>
      )
    else if (!this.state.loadComplete)
      return (
        <View style={{flex:1}}>
          <Header
            statusBarProps={{backgroundColor:'#089',translucent:true}}
            containerStyle={{backgroundColor:'#3ab'}}
            placement="left"
            centerComponent={{text:'Import progress', style:{fontSize:20,color:'white'}}}
            />
          <View style={styles.container}>
            <Text style={styles.heading}>Importing archive...</Text>
            <Text style={styles.simpleText}>{this.state.progress}...</Text>
          </View>
        </View>
      )
    else 
      return (
        <View style={{flex:1}}>
          <Header
            statusBarProps={{backgroundColor:'#089',translucent:true}}
            containerStyle={{backgroundColor:'#3ab'}}
            placement="left"
            leftComponent={{ icon: 'arrow-back', color: '#fff', underlayColor:'transparent', onPress:()=>this.props.onDone(null)}}
            centerComponent={{text:'Import complete', style:{fontSize:20,color:'white'}}}
            />
          <View style={styles.container}>
            <Text style={styles.heading}>Import complete</Text>
            <Text style={styles.simpleText}>{this.state.loadSuccess?'With success':'With error :C'}</Text>
            <View style={{margin:20}}>
              <Icon
                color={this.state.loadSuccess?'#3ab':'#e00'}
                size={150}
                reverse
                name={this.state.loadSuccess?'check':'clear'}
                />
            </View>
            {
              this.state.device ? (
                <View style={{alignSelf:'center'}}>
                  <Button 
                    buttonStyle={styles.button}
                    onPress={() => this.props.onDone(this.state.device.code)}
                    title='Show device'/>
                </View>
              ) : null
            }
          </View>
        </View>
      )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'white',
    overflow:'hidden'
  },
  previewPane: {
    minHeight:500,
    minWidth:150,
    alignItems:'center',
    justifyContent:'center'
  },
  portsPane: {
    flex: 1
  },
  detailsPane: {
    minHeight:200,
    backgroundColor:'#aff'
  },
  camButton: {
    position: 'relative',
    alignItems: 'center',
    padding: 10,
    width:200,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#3ab',
    padding: 10,
    width:300,
    marginTop:16
  },
  heading: { 
    color: 'black', 
    fontSize: 24, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 30 
  },
  simpleText: { 
    color: 'black', 
    fontSize: 20, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 16
  },
  errorText: {
    color: 'red', 
    fontSize: 16,  
  }
});