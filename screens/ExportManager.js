import React, { Component } from 'react';
import { ScrollView, BackHandler, Alert, Text, View, Linking, TouchableHighlight, PermissionsAndroid, Platform, StyleSheet} from 'react-native';
import { Icon, ButtonGroup, Input, Header, Button } from 'react-native-elements';
import { InventoryList } from './InventoryList';
import { DataMgr } from './DataMgr'
import { InputRow } from './InputRow'
import { zip, unzip, subscribe } from 'react-native-zip-archive'; 
import { Misc } from './Misc'
var RNFetchBlob = require('react-native-fetch-blob').default;
var RNFS = require('react-native-fs');

const exportModes = {
  'local':0,
  'remote':1
}

const revExportModes = [
  'local',
  'remote'
]

export class ExportManager extends Component {
  constructor() {
    super();
    this.state = {
      //variable to hold the qr value
      device: null,
      passphrase: '',
      destination:'',
      exportMode:'local',
      gotData:false,
      saveComplete:true,
      saveSuccess:false,
      progress:'error',
      uploadedFileName:null
    };
  }
  isDestinationValid(){
    if (this.state.exportMode == 'local')
      return !this.state.destination.match(/\/|^$/);
    if (this.state.exportMode == 'remote')
      return this.state.destination.match(/^https?:\/\/.+/)
  }
  isPassphraseValid(){
    return this.state.passphrase.length >= 8
  }
  uploadBundle(cb){
    var dstPath = RNFS.DocumentDirectoryPath+'/upload-bundle.hiv';
    this.exportBundle(dstPath,(success,err)=>{
      this.setState({progress:'Uploading bundle'});
      RNFetchBlob.fetch('POST', this.state.destination+'/hivs', {
        'Content-Type' : 'application/octet-stream',
      }, RNFetchBlob.wrap(dstPath))
      .uploadProgress((written,total)=>{
        this.setState({progress:`Upload progress: ${(100*written/total).toFixed(1)}`});
      }).then((res) => {
        if (res.respInfo.status == 200){
          var fileName;
          try {
            fileName = JSON.parse(res.text()).fileName;
          } catch (e) {
            cb(false,'Could not parse server response '+e);
          }
          this.setState({uploadedFileName:fileName});
          cb(true);
        } else
          cb(false,'Network error: '+res.respInfo.status);
      }).catch((err) => {
        cb(false,err);
      })
    })
  }
  saveBundle(cb){
     Misc.getPermission([
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE
      ],(res)=>{
        if(res){
          var dstPath = RNFS.ExternalStorageDirectoryPath+'/'+this.state.destination+'.hiv';
          this.exportBundle(dstPath,cb);
        } else
          cb(false,'Required permissions have not been granted')
      });
  }
  exportBundle(dstPath,cb){
    var devPath = RNFS.DocumentDirectoryPath+'/attachments/'+this.props.realmName+'/'+this.state.device.code+'/', 
        srcPath = RNFS.DocumentDirectoryPath+'/export-'+Date.now()+'-'+(~~(Math.random()*1000000000));
    
    RNFS.exists(devPath)
      .then((success)=>{
        if (!success)
          cb(false,'Device not found');
        else {
          RNFS.mkdir(srcPath)
            .then((success)=>{
              RNFS.mkdir(srcPath+'/attachments/')
                .then((success)=>{
                  this.setState({progress:'saving device data'});
                  var encDev = DataMgr.encAES(this.state.passphrase,JSON.stringify(this.state.device));
                  RNFS.writeFile(srcPath+'/device.hivd',encDev,'ascii')
                    .then((success)=>{
                      this.copyDir(devPath,srcPath+'/attachments/',(success,err)=>{
                        if (!success)
                          cb(false,err);
                        else {
                          zip(srcPath, dstPath)
                            .then((path)=>{
                              this.setState({progress:'cleanup'});
                              this.deleteDir(srcPath+'/attachments/',(success,err)=>{
                                if (!success)
                                  cb(false,err);
                                else  {
                                  this.deleteDir(srcPath,(success,err)=>{
                                    if (!success)
                                      cb(false,err);
                                    else {
                                      cb(true);
                                    }
                                  })
                                }
                              })
                            }).catch((err)=>{
                              cb(false,err);
                            });
                        }
                      });
                    }).catch((err)=>{
                      cb(false,err);
                    })
                }).catch((err)=>{
                  cb(false,err);
                })
            }).catch((err)=>{
              cb(false,err);
            })
        }
      }).catch((err)=>{
        cb(false,err);
      })
  }
  copyDir(src, dst, cb){
    RNFS.readdir(src)
      .then((success)=>{
        var cnt = success.length,
            errors = [],
            onDone = ()=>{
              this.setState({progress:`copying files ${success.length-cnt}/${success.length}`});
              if (cnt-- == 0){
                if (errors.length == 0)
                  cb(true);
                else
                  cb(false,errors);
              }
            }
        success.forEach(d=>{
          RNFS.copyFile(src+'/'+d, dst+'/'+d)
            .then((success)=>{
              onDone();
            }).catch((err)=>{
              errors.push(err);
              onDone();
            })
        })
        onDone();
      }).catch((err)=>{
        cb(false,err);
      })
  }
  deleteDir(dir, cb){
    RNFS.readdir(dir)
      .then((success)=>{
        var cnt = success.length,
            errors = [],
            onDone = ()=>{
              this.setState({progress:`cleanup ${success.length-cnt}/${success.length}`});
              if (cnt-- == 0){
                if (errors.length == 0)
                  RNFS.unlink(dir)
                    .then((success)=>{
                      cb(true);
                    }).catch((err)=>{
                      cb(false,[err]);
                    });
                else
                  cb(false,errors);
              }
            }
        success.forEach(d=>{
          RNFS.unlink(dir+'/'+d)
            .then((success)=>{
              onDone();
            }).catch((err)=>{
              errors.push(err);
              onDone();
            })
        })
        onDone();
      }).catch((err)=>{
        cb(false,err);
      })
  }
  cleanup(cb){
    var srcPath = RNFS.DocumentDirectoryPath;
    RNFS.readdir(RNFS.DocumentDirectoryPath)
      .then((success)=>{
        success = success.filter(d=>d.match(/^export-/));
        var cnt = success.length,
            errors = [],
            onDone = ()=>{
              if (cnt-- == 0){
                if (errors.length == 0)
                  cb(true,success.length);
                else
                  cb(false,errors);
              }
            }
        success.forEach(d=>{
          RNFS.exists(RNFS.DocumentDirectoryPath+'/'+d+'/attachments/')
            .then((exists)=>{
              if (exists)
                this.deleteDir(RNFS.DocumentDirectoryPath+'/'+d+'/attachments/',(success,err)=>{
                  if (success)
                    this.deleteDir(RNFS.DocumentDirectoryPath+'/'+d, (success,err)=>{
                      if (!success)
                        err.forEach(e=>errors.push(e));
                      onDone();
                    })
                  else {
                    err.forEach(e=>errors.push(e));
                    onDone();
                  }
                });
              else
                this.deleteDir(RNFS.DocumentDirectoryPath+'/'+d, (success,err)=>{
                  if (!success)
                    err.forEach(e=>errors.push(e));
                  onDone();
                });
            }).catch((err)=>{
              errors.push(err)
              onDone();
            })
        })
        onDone();
      }).catch((err)=>{
        console.warn('Error during cleanup');
      })
  }
  componentWillMount() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', ()=>{
      if (this.state.saveComplete)
        this.props.onDone(null); 
      return true;});
    this.cleanup((success,ret)=>{
      if (success && ret>0)
        Alert.alert(
          'Notice',
          `Cleaned up ${ret} rougue temporary files left after recent export crash`
        );
      else if (!success)
        Alert.alert(
          'Error',
          'Error occured during rougue temporary files cleanup:'+err.join('\n')
        );
    });
  }
  componentWillUnmount() {
    this.backHandler.remove();
  }
  render() {
    let displayModal;
    //If qrvalue is set then return this view
    if (!this.state.device)
      return (
        <InventoryList
          title='Export device'
          inventory={this.props.inventory}
          allowNew={false}
          allowDelete={false}
          onSelect={(code)=>{
            if (code == null)
              this.props.onDone(null);
            else
              this.setState({device:this.props.inventory.hwList.filter(d=>d.code==code)[0]});
          }}/>
      )
    else if (this.state.gotData == false)
      return (
        <View style={{flex:1}}>
          <Header
            statusBarProps={{backgroundColor:'#089',translucent:true}}
            containerStyle={{backgroundColor:'#3ab'}}
            placement="left"
            leftComponent={{ icon: 'arrow-back', color: '#fff', underlayColor:'transparent', onPress:()=>this.props.onDone(null)}}
            centerComponent={{text:'Export settings', style:{fontSize:20,color:'white'}}}
            />
          <View style={styles.container}>
            <View style={{padding:50, flex:1, width:'100%'}}>
              <Text>Passphrase for bundle encryption</Text>
              <Input
                defaultValue={this.state.passphrase}
                placeholder="Passphrase"
                secureTextEntry={true}
                onChangeText={(text)=>this.setState({passphrase:text})}
                />
              {
                !this.isPassphraseValid() ? (
                  <Text style={styles.errorText}>Passphrase must be at least 8 characters long</Text>
                ) : null
              }
              <ButtonGroup
                selectedIndex={exportModes[this.state.exportMode]}
                buttons={revExportModes}
                onPress={(i)=>this.setState({exportMode:revExportModes[i]})}/>
              <Text>{this.state.exportMode == 'local'?'File name':'Server address'}</Text>
              <Input
                defaultValue={this.state.destination}
                placeholder={this.state.exportMode == 'local'?'File name':'Address'}
                onChangeText={(text)=>this.setState({destination:text})}
                />
              {
                this.state.exportMode == 'remote' && !this.isDestinationValid() ? (
                  <Text style={styles.errorText}>Invalid server address (Note: protocol is required)</Text>
                ) : this.state.exportMode == 'local' && !this.isDestinationValid() ? (
                  <Text style={styles.errorText}>Illegal filename</Text>
                ) : null
              }
              {
                this.state.exportMode == 'local' && this.isDestinationValid() ? (
                  <React.Fragment>
                    <Text style={styles.simpleText}>Bundle will be saved to:</Text>
                    <Text>{RNFS.ExternalStorageDirectoryPath+'/'+this.state.destination+'.hiv'}</Text>
                  </React.Fragment>
                ) : null
              }
              <View style={{position:'absolute',bottom:0,right:0,padding:20}}>
                <Icon
                  reverse
                  name={this.state.exportMode=='local'?'archive':'cloud-upload'}
                  disabled={!this.isDestinationValid() || !this.isPassphraseValid()}
                  onPress={()=>{
                    this.setState({gotData:true, progress:'preparing', saveSuccess:false, saveComplete:false}, ()=>{
                      var cb = (success,err)=>{
                        this.setState({progress:'done',saveComplete:true, saveSuccess:success});
                        if (!success)
                          Alert.alert(
                            'Error',
                            'Export finished with following error:\n'+(Array.isArray(err)?err.join('\n'):err)
                          );
                      };
                      if (this.state.exportMode == 'local')
                        this.saveBundle(cb);
                      else
                        this.uploadBundle(cb);
                    });
                  }}/>
              </View>
            </View>
          </View>
        </View>
      )
    else if (!this.state.saveComplete)
      return (
        <View style={{flex:1}}>
          <Header
            statusBarProps={{backgroundColor:'#089',translucent:true}}
            containerStyle={{backgroundColor:'#3ab'}}
            placement="left"
            centerComponent={{text:'Export progress', style:{fontSize:20,color:'white'}}}
            />
          <View style={styles.container}>
            <Text style={styles.heading}>Exporting archive...</Text>
            <Text style={styles.simpleText}>{this.state.progress}...</Text>
          </View>
        </View>
      )
    else 
      return (
        <View style={{flex:1}}>
          <Header
            statusBarProps={{backgroundColor:'#089',translucent:true}}
            containerStyle={{backgroundColor:'#3ab'}}
            placement="left"
            leftComponent={{ icon: 'arrow-back', color: '#fff', underlayColor:'transparent', onPress:()=>this.props.onDone(null)}}
            centerComponent={{text:'Export complete', style:{fontSize:20,color:'white'}}}
            />
          <View style={styles.container}>
            <Text style={styles.heading}>Export complete</Text>
            <Text style={styles.simpleText}>{this.state.saveSuccess?'With success':'With error :C'}</Text>
            <View style={{margin:20}}>
              <Icon
                color={this.state.saveSuccess?'#3ab':'#e00'}
                size={150}
                reverse
                name={this.state.saveSuccess?'check':'clear'}
                />
            </View>
            {
              this.state.uploadedFileName ? (
                <Text style={styles.simpleText}>Uploaded file: {this.state.uploadedFileName}</Text>
              ) : null
            }
          </View>
        </View>
      )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'white',
    overflow:'hidden'
  },
  previewPane: {
    minHeight:500,
    minWidth:150,
    alignItems:'center',
    justifyContent:'center'
  },
  portsPane: {
    flex: 1
  },
  detailsPane: {
    minHeight:200,
    backgroundColor:'#aff'
  },
  camButton: {
    position: 'relative',
    alignItems: 'center',
    padding: 10,
    width:200,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#2c3539',
    padding: 10,
    width:300,
    marginTop:16
  },
  heading: { 
    color: 'black', 
    fontSize: 24, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 30 
  },
  simpleText: { 
    color: 'black', 
    fontSize: 20, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 16
  },
  errorText: {
    color: 'red', 
    fontSize: 16,  
  }
});