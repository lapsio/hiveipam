import React, { Component } from 'react';
import { FlatList, ScrollView, BackHandler, Alert, Text, View, Linking, TouchableHighlight, PermissionsAndroid, Platform, StyleSheet} from 'react-native';
import { Button, Input, ListItem } from 'react-native-elements';
import { DevicePortGroup } from './DevicePortGroup'

export class DeviceSection extends Component {
  constructor() {
    super();
    this.state = {
    };
  }
  componentWillMount() {
    /*this.backHandler = BackHandler.addEventListener('hardwareBackPress', ()=>{
      this.props.callback(null); 
      return true;});*/
  }
  componentWillUnmount() {
    /*this.backHandler.remove();*/
  }
  
  render() {
    let displayModal;
    //If qrvalue is set then return this view
    return (
      <View
        style={{
          flex:this.props.size,
          borderColor:this.props.device.color,
          borderBottomWidth:1,
          borderRightWidth:1,
          flexDirection:this.props.direction
        }}>
        {
          this.props.content.map((c,i)=>(
            c.type=='section'?
            (
              <DeviceSection
                key={i}
                device={this.props.device}
                content={c.content}
                size={c.size}
                direction={c.direction}
                level={this.props.level+1}
                />
            ) : c.type=='portgroup'? (
              <DevicePortGroup
                key={i}
                device={this.props.device}
                size={c.size}
                portdefaults={c.portdefaults}
                width={c.width}
                height={c.height}
                groupId={c.gid}
                startId={c.sid}
                direction={c.direction}
                flip={c.flip}
                level={this.props.level+1}
                />
            ) : (
              <View style={{flex:1,backgroundColor:'#f00'}}>
              </View>
            )
          ))
        }
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'white'
  },
   inputrow: {
    flexDirection: 'row',
    paddingHorizontal:30,
    paddingBottom: 15
  },
  inputbutton: {
    alignItems: 'center',
    backgroundColor: '#2c3539',
    paddingVertical: 10,
    paddingHorizontal: 30,
    marginTop:16
  },
  inputinput: {
    flex:1,
    marginTop:16,
    marginRight:20
  },
  camButton: {
    position: 'relative',
    alignItems: 'center',
    padding: 10,
    width:200,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#2c3539',
    padding: 10,
    width:300,
    marginTop:16
  },
  heading: { 
    color: 'black', 
    fontSize: 24, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 30 
  },
  simpleText: { 
    color: 'black', 
    fontSize: 20, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 16
  }
});