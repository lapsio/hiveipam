import React, { Component } from 'react';
import { FlatList, ScrollView, BackHandler, Alert, Text, View, Linking, TouchableHighlight, PermissionsAndroid, Platform, StyleSheet} from 'react-native';
import { ButtonGroup, CheckBox, Slider, Button, Input, ListItem } from 'react-native-elements';
import { DeviceCredentialsEditor } from './DeviceCredentialsEditor';
import { Collapsible } from './Collapsible'
import { DeviceListPropEditor } from './DeviceListPropEditor'

const mediums={
  'copper':0,
  'sfp':1,
  'serial':2,
  'aux':3
}

const revMediums=[
  'copper',
  'sfp',
  'serial',
  'aux'
]

const portTypes={
  'mgmt':0,
  'aux':1,
  'data':2
}

const revPortTypes=[
  'mgmt',
  'aux',
  'data'
]

const portdefaults = {
  medium:'sfp',
  vlans:[],
  ips:[],
  type:'data',
  notes:''
}

export class DevicePortsEditor extends Component {
  constructor() {
    super();
    this.state = {
      openByDef:false
    };
  }
  updateSelf(self, silent) {
    for (var key in self)
      this.props.device[key]=self[key];
    if (!silent)
      this.props.onEdit();
  }
  collapseToggle(key) {
    var newState={};
    newState[key]=!this.state[key];
    this.setState(newState);
  }
  updateLocation(location, silent){
    for (var key in location)
      this.props.device.location[key]=location[key];
    if (!silent)
      this.props.onEdit();
  }
  parseLocation(){
    var Upos = Number(this.props.device.location.U);
    return (isNaN(Upos)?
      'unknown':
      this.props.device.U>1?
        `${Upos}-${Upos+this.props.device.U-1}`:
        Upos);
  }
  clashPorts(section){
    var o = {};
    if (section.type=='portgroup'){
      o[section.gid]={};
      for (var i = 0 ; i < section.width*section.height ; ++i)
        o[section.gid][i+section.sid]=section.portdefaults;
    } else { 
      section.content.map(c=>this.clashPorts(c)).forEach(c=>{
        for (var group in c)
          for (var port in c[group])
            (o[group]||(o[group]={}))[port]=c[group][port];
      });
    }
    return o;
  }
  getPortInfo(){
    var o3 = this.props.device.portinfo,
        o2 = this.clashPorts(this.props.device),
        o1 = portdefaults;
        
    return [o1,o2,o3];
  }
  mergePort(group, port, defaults, groupdefaults, overrides){
    var portinfo = {},
        groupdefaults = groupdefaults[group][port],
        overrides = ((overrides[group]||{})[port]||{});
    portinfo._src={};
    portinfo._src.overrides = Object.keys(overrides);
    portinfo._src.groupdefaults = Object.keys(groupdefaults)
      .filter(k=>!portinfo._src.overrides.includes(k));
    portinfo._src.defaults = Object.keys(portdefaults)
      .filter(k=>!portinfo._src.overrides.includes(k) &&
                 !portinfo._src.groupdefaults.includes(k));
    
    portinfo = {...portinfo, ...defaults, ...groupdefaults, ...overrides};

    return portinfo;
  }
  getInfoSrc(mergedPort, prop){
    return (mergedPort._src.defaults.includes(prop)?'defaults':
      mergedPort._src.groupdefaults.includes(prop)?'groupdefaults':
        mergedPort._src.overrides.includes(prop)?'overrides':'unknown');
  }
  getPortMap(groupdefaults){
    var o = groupdefaults;
    return (Object.keys(o)
      .map(group=>({
        name:group,
        ports:Object.keys(o[group])
          .map(k=>({number:k}))
      }))
    );
  }
  hasOverrides(group, port){
    return (group in this.props.device.portinfo && 
            port in this.props.device.portinfo[group]);
  }
  override(fullportinfo, group, port, prop, value){
    if (!this.props.device.portinfo[group])
      this.props.device.portinfo[group]={};
    if (!this.props.device.portinfo[group][port])
      this.props.device.portinfo[group][port]={};
    this.props.device.portinfo[group][port][prop]=value;
    this.props.onEdit();
  }
  reset(group, port){
    if (this.props.device.portinfo[group])
      delete this.props.device.portinfo[group][port];
    this.props.onEdit();
  }
  componentWillMount() {
    /*this.backHandler = BackHandler.addEventListener('hardwareBackPress', ()=>{
      this.props.callback(null); 
      return true;});*/
  }
  componentWillUnmount() {
    /*this.backHandler.remove();*/
  }
  
  render() {
    let displayModal;
    //If qrvalue is set then return this view
    return (
      <Collapsible
        beginCollapsed={true}
        style={styles.collapsible}
        titleStyle={styles.collapsibleHead}
        title="Edit interfaces >">
        <CheckBox
          checked={this.state.openByDef}
          onPress={()=>{this.setState({openByDef:!this.state.openByDef})}}
          title='Open ports by default'/>
        {
          (()=>{
            var fullportinfo = this.getPortInfo();
            return this.getPortMap(fullportinfo[1]).map((group,i)=>(
              <Collapsible
                key={i}
                beginCollapsed={true}
                style={styles.collapsible}
                titleStyle={styles.collapsibleHead}
                title={`${group.name} >`}>
                {
                  group.ports.map((port,j)=>(
                    <Collapsible
                      key={j}
                      beginCollapsed={!this.state.openByDef}
                      style={styles.collapsible}
                      icon='cancel'
                      titleStyle={styles.collapsibleHead}
                      title={`${group.name}.${port.number} >`}
                      allowDelete={this.hasOverrides(group.name,port.number)}
                      onDelete={()=>this.reset(group.name,port.number)}>
                      {
                        (()=>{
                          var portinfo = this.mergePort(group.name,port.number,
                                          fullportinfo[0],fullportinfo[1],fullportinfo[2]);
                          return (
                            <React.Fragment>
                              <Text>[{this.getInfoSrc(portinfo,'medium')[0].toUpperCase()}] Medium: {portinfo.medium}</Text>
                              <ButtonGroup
                                selectedIndex={mediums[portinfo.medium]}
                                buttons={revMediums}
                                onPress={(i)=>this.override(fullportinfo,group.name,port.number,'medium',revMediums[i])}
                                />
                              <Text>[{this.getInfoSrc(portinfo,'type')[0].toUpperCase()}] Port type: {portinfo.type}</Text>
                              <ButtonGroup
                                selectedIndex={portTypes[portinfo.type]}
                                buttons={revPortTypes}
                                onPress={(i)=>this.override(fullportinfo,group.name,port.number,'type',revPortTypes[i])}
                                />
                              <Text>[{this.getInfoSrc(portinfo,'vlans')[0].toUpperCase()}] VLANs:</Text>
                              <DeviceListPropEditor
                                list={portinfo.vlans||[]}
                                allowDelete={true}
                                placeholder='VLAN'
                                onAdd={(vlan)=>portinfo.vlans.includes(vlan)?Alert.alert('Error','VLAN already exists'):
                                  this.override(fullportinfo,group.name,port.number,'vlans',portinfo.vlans.concat(vlan))}
                                onDelete={(vlan)=>this.override(fullportinfo,group.name,port.number,
                                  'vlans',portinfo.vlans.filter(v=>v!=vlan))}
                                />
                              <Text>[{this.getInfoSrc(portinfo,'ips')[0].toUpperCase()}] IPs:</Text>
                              <DeviceListPropEditor
                                list={portinfo.ips||[]}
                                allowDelete={true}
                                placeholder='Address'
                                onAdd={(ip)=>portinfo.ips.includes(ip)?Alert.alert('Error','Address already exists'):
                                  this.override(fullportinfo,group.name,port.number,'ips',portinfo.ips.concat(ip))}
                                onDelete={(ip)=>this.override(fullportinfo,group.name,port.number,
                                  'ips',portinfo.ips.filter(i=>i!=ip))}
                                />
                              <Text>Notes</Text>
                              <Input
                                defaultValue={portinfo.notes}
                                containerStyle={styles.inputinput}
                                placeholder='Notes'
                                onChangeText={(text)=>this.override(fullportinfo,group.name,port.number,'notes',text)}/>
                            </React.Fragment>
                          )
                        })()
                      }
                    </Collapsible>
                  ))
                }
              </Collapsible>
            ))
          })()
        }
      </Collapsible>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'white'
  },
   inputrow: {
    flexDirection: 'row',
    paddingHorizontal:30,
    paddingBottom: 15
  },
  inputbutton: {
    alignItems: 'center',
    backgroundColor: '#2c3539',
    paddingVertical: 10,
    paddingHorizontal: 30,
    marginTop:16
  },
  inputinput: {
    flex:1,
    marginTop:16,
    marginRight:20
  },
  camButton: {
    position: 'relative',
    alignItems: 'center',
    padding: 10,
    width:200,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#2c3539',
    padding: 10,
    width:300,
    marginTop:16
  },
  heading: { 
    color: 'black', 
    fontSize: 24, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 30 
  },
  simpleText: { 
    color: 'black', 
    fontSize: 20, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 16
  },
  collapsible:{
    borderLeftWidth:1,
    paddingLeft:10,
    borderColor:'#aaa',
    paddingBottom:15
  },
  collapsibleHead: { 
    color: '#777', 
    fontSize: 20,  
    padding: 16
  }
});