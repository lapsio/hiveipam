import React, { Component } from 'react';
import { ScrollView, BackHandler, Alert, Text, View, Linking, TouchableHighlight, PermissionsAndroid, Platform, StyleSheet} from 'react-native';
import { Button } from 'react-native-elements';
import { Editor } from './Editor';
import { InventoryList } from './InventoryList';

export class Browser extends Component {
  constructor() {
    super();
    this.state = {
      //variable to hold the qr value
      item: null,
      inventory: null
    };
  }
  editItem(newItem) {
    if (newItem != null){
      var item = this.cinventory.hwList.filter(hw => hw.code == this.state.item)[0]||null,
          index = this.cinventory.hwList.indexOf(item);
      if (index == -1)  //new item
        this.cinventory.hwList.push(newItem);
      else
        this.cinventory.hwList.splice(index,1,newItem);
      this.props.onEdit(this.cinventory);
    }
    this.setState({item: null});
  }
  deleteItem(code){
    var item = this.cinventory.hwList.filter(hw => hw.code == code)[0]||null,
        index = this.cinventory.hwList.indexOf(item);
    if (index != -1){  //exists
      Alert.alert(
        `Remove device ${item.name} [${item.code}]?`,
        "This operation cannot be undone",
        [
          {text: 'Yeh', onPress: () => {
            this.cinventory.hwList.splice(index,1);
            this.props.onEdit(this.cinventory);
          }},
          {text: 'Nuh-uh'}
        ]
      );
    } else
      console.warn('Error during delete? - not found');
    //this.forceUpdate();
  }
  componentWillMount() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', ()=>{
      this.props.onDone(null); 
      return true;});
    this.setState({item: this.props.item});
    //this.cinventory = JSON.parse(JSON.stringify(this.props.inventory));
    this.cinventory = this.props.inventory;
  }
  componentWillUnmount() {
    this.backHandler.remove();
  }
  
  render() {
    let displayModal;
    //If qrvalue is set then return this view
    if (this.state.item) {
      return (
        <Editor
          realmName={this.props.realmName}
          inventory={this.cinventory}
          code={this.state.item}
          device={this.cinventory.hwList.filter(hw => hw.code == this.state.item)[0]||null}
          onDone={(newItem) => this.editItem(newItem)}
          />
      )
    } else
      return (
        <InventoryList
          title={`${this.props.realmName} inventory`}
          inventory={this.cinventory}
          allowNew={true}
          allowDelete={true}
          onDelete={(code)=>this.deleteItem(code)}
          onSelect={(code)=>{
            if (code == null) 
              this.props.onDone(false);
            else
              this.setState({item: code})
          }}
          />
      )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'#ffa'
  },
  previewPane: {
    minHeight:500,
    minWidth:200,
    backgroundColor:'#faa',
    alignItems:'center'
  },
  portsPane: {
    flex: 1,
    backgroundColor:'#faf'
  },
  detailsPane: {
    minHeight:200,
    backgroundColor:'#aff'
  },
  camButton: {
    position: 'relative',
    alignItems: 'center',
    padding: 10,
    width:200,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#2c3539',
    padding: 10,
    width:300,
    marginTop:16
  },
  heading: { 
    color: 'black', 
    fontSize: 24, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 30 
  },
  simpleText: { 
    color: 'black', 
    fontSize: 20, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 16
  }
});