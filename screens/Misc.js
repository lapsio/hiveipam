import React, { Component } from 'react';
import { BackHandler, Alert, Text, View, Linking, TouchableHighlight, PermissionsAndroid, Platform, StyleSheet} from 'react-native';
import { Button } from 'react-native-elements';

export class Misc {
  constructor() {
  }
  static getPermission(permissions, cb) {
    var that = this;
    
    if(Platform.OS === 'android'){
      async function requestPermission() {
        try {
          console.log('poke1');
          const granted = await PermissionsAndroid.requestMultiple(permissions)
          console.log('poke2');
          if (Object.keys(granted)
              .filter(k=>granted[k]!=PermissionsAndroid.RESULTS.GRANTED)
              .length==0) {
            cb(true,granted);
          } else {
            cb(false,granted);
          }
        } catch (err) {
          alert("Permission error "+err);
          console.warn(err);
          cb(false,null);
        }
      }
      requestPermission();
    } else
      cb(true,null);
  }
}