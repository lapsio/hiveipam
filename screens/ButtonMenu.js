import React, { Component } from 'react';
import { BackHandler, Alert, Text, View, Linking, TouchableHighlight, PermissionsAndroid, Platform, StyleSheet} from 'react-native';
import { Button } from 'react-native-elements';

export class ButtonMenu extends Component {
  constructor() {
    super();
    this.state = {
    };
  }
  render() {
    return (
      <View style={styles.container}>
      {
        this.props.buttons.map((m,i)=>(
          <Button 
            key={i}
            buttonStyle={styles.button}
            onPress={() => this.props.onSelect(i)}
            title={m}/>
        ))
      }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'white'
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#3ab',
    padding: 10,
    width:300,
    marginTop:16
  },
  heading: { 
    color: 'black', 
    fontSize: 24, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 30 
  },
  simpleText: { 
    color: 'black', 
    fontSize: 20, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 16
  }
});