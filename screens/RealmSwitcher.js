import React, { Component } from 'react';
import { FlatList, ScrollView, BackHandler, Alert, Text, View, Linking, TouchableHighlight, PermissionsAndroid, Platform, StyleSheet} from 'react-native';
import { Header, Button, Input, ListItem } from 'react-native-elements';
import { InputRow } from './InputRow'

export class RealmSwitcher extends Component {
  constructor() {
    super();
    this.state = {
      filter:''
    };
  }
  componentWillMount() {
    /*this.backHandler = BackHandler.addEventListener('hardwareBackPress', ()=>{
      this.props.callback(null); 
      return true;});*/
  }
  componentWillUnmount() {
    /*this.backHandler.remove();*/
  }
  
  render() {
    let displayModal;
    //If qrvalue is set then return this view
    return (
      <View style={{flex:1}}>
        <Header
          statusBarProps={{backgroundColor:'#089',translucent:true}}
          containerStyle={{backgroundColor:'#3ab'}}
          placement="left"
          centerComponent={{text:"Realms", style:{fontSize:20,color:'white'}}}
          />
        <View style={styles.container}>
          <ScrollView style={{alignSelf:'stretch'}}>
          <View style={{alignSelf:'stretch',paddingHorizontal:30}}>
          {
            this.props.realms.length?
              this.props.realms
                .map((r,i) => [r,i])
                .filter((rr, i) => rr[0].toLowerCase().indexOf(this.state.filter)!=-1)
                .map((rr,i) => (
                <ListItem
                  key={i}
                  title={rr[0]}
                  onPress={() => this.props.onSelectRealm(rr[1])}
                  rightElement={
                    <Button  
                      type='clear' 
                      icon={{name:'clear'}}
                      onPress={() => this.props.onDelRealm(rr[1])}/>
                  }/>
              )) : null
          }
          </View>
          </ScrollView>
          <InputRow
            buttonIcon='add'
            placeholder="Realm name"
            onChangeText={(text)=>{this.setState({filter:text.toLowerCase()})}}
            onButtonPress={(newRealm)=>{this.props.onAddRealm(newRealm)}}/>
        </View>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'white'
  },
   inputrow: {
    flexDirection: 'row',
    paddingHorizontal:30,
    paddingBottom: 15
  },
  inputbutton: {
    alignItems: 'center',
    backgroundColor: '#2c3539',
    paddingVertical: 10,
    paddingHorizontal: 30,
    marginTop:16
  },
  inputinput: {
    flex:1,
    marginTop:16,
    marginRight:20
  },
  camButton: {
    position: 'relative',
    alignItems: 'center',
    padding: 10,
    width:200,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#2c3539',
    padding: 10,
    width:300,
    marginTop:16
  },
  heading: { 
    color: 'black', 
    fontSize: 24, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 30 
  },
  simpleText: { 
    color: 'black', 
    fontSize: 20, 
    alignSelf: 'center', 
    padding: 10, 
    marginTop: 16
  }
});